<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-10-27
  Time: 18:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- login success -->
<section>
    <div class="col text-center mb-5">
        <div class="col-sm-8 col-md-6 col-lg-4 mx-auto d-block eb-sign-success">
            <h1>Zostałeś zarejestrowany</h1>
            <a href="/login"> Możesz się zalogować</a>
        </div>
    </div>
</section>


<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>
