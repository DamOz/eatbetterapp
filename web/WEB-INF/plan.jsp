<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-10-27
  Time: 18:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"></jsp:include>

<!-- summary -->
<section>
    <div class="container eb-plan">
        <form action="plan" method="post">
            <div class="text-center eb-plan-main">
                <c:set var="user" value="${requestScope.user}"> </c:set>
                <c:set var="summary" value="${requestScope.summary}"> </c:set>
                <c:set var="calories" value="${requestScope.calories}"> </c:set>
                <h1 class="text-uppercase eb-plan-header-main">Plan żywieniowy dla:</h1>
                <h1 class="text-uppercase mb-5 mt-5 eb-plan-header-name"><c:out value="${user.username}"></c:out> :</h1>
                <div class="eb-plan-form">
                    <div class="eb-plan-form-calories">
                        <h3>Kaloryczność i gramatura</h3>
                        <div class="row mt-5">
                            <div class="col-9 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                <p>Twoje dzienne zapotrzebowanie kaloryczne podane w kcal:
                            </div>
                            <div class="col-3 d-block mx-auto text-left eb-plan-form-text">
                                <p><c:out value="${calories.caloriesTotal}"></c:out> Kcal</p>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-9 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                <p>Twoje dzienne zapotrzebowanie na wodę podane w litrach to:</p>
                            </div>
                            <div class="col-3 d-block mx-auto text-left eb-plan-form-text">
                                <p><c:out value="${calories.caloriesWater}"> </c:out> litra</p>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-9 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                <p>Ilość gramów białka na posiłek to:</p>
                            </div>
                            <div class="col-3 d-block mx-auto  text-left eb-plan-form-text">
                                <p><c:out value="${calories.caloriesMealProtein}"></c:out> g</p>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-9 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                <p>Ilość gramów węglowodanów na posiłek to:</p>
                            </div>
                            <div class="col-3 d-block mx-auto text-left eb-plan-form-text">
                                <p><c:out value="${calories.caloriesMealCarbo}"></c:out> g</p>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-9 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                <p>Ilość gramów tłuszczy na posiłek to:</p>
                            </div>
                            <div class="col-3 d-block mx-auto text-left eb-plan-form-text">
                                <p><c:out value="${calories.caloriesMealFat}"></c:out> g</p>
                            </div>
                        </div>
                    </div>
                    <div class="eb-plan-form-pattern">
                        <h3>Ogólny wzór na posiłek przy samodzielnym komponowaniu</h3>
                        <c:set var="meal3" value="3"></c:set>
                        <c:set var="meal4" value="4"></c:set>
                        <c:set var="meal5" value="5"></c:set>
                        <c:set var="firstMeal1" value="0.2"></c:set>
                        <c:set var="secondMeal1" value="0.5"></c:set>
                        <c:set var="thirdMeal1" value="0.3"></c:set>
                        <c:set var="firstMeal2" value="0.15"></c:set>
                        <c:set var="secondMeal2" value="0.2"></c:set>
                        <c:set var="thirdMeal2" value="0.4"></c:set>
                        <c:set var="fourthMeal2" value="0.25"></c:set>
                        <c:set var="firstMeal3" value="0.1"></c:set>
                        <c:set var="secondMeal3" value="0.2"></c:set>
                        <c:set var="thirdMeal3" value="0.3"></c:set>
                        <c:set var="fourthMeal3" value="0.3"></c:set>
                        <c:set var="fifthMeal3" value="0.1"></c:set>
                        <c:choose>
                            <c:when test="${summary.metricMeals == meal3}">
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek I:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * firstMeal1}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek II:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * secondMeal1}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek III:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * thirdMeal1}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${summary.metricMeals == meal4}">
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek I:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * firstMeal2}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek II:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * secondMeal2}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek III:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * thirdMeal2}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek IV:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * fourthMeal2}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${summary.metricMeals == meal5}">
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek I:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * firstMeal3}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek II:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * secondMeal3}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek III:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern"> Porcja
                                        białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * thirdMeal3}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek IV:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * fourthMeal3}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Posiłek V:
                                    </div>
                                    <div class="col-9 d-block mx-auto  text-left eb-plan-form-category-pattern">
                                        Porcja białka + porcja węglowodanów +
                                        <c:out value="${calories.caloriesVegetables * fifthMeal3}"></c:out>
                                        g warzyw + porcja tłuszczy
                                    </div>
                                </div>
                            </c:when>
                        </c:choose>
                    </div>
                    <div class="eb-plan-form-products">
                        <h3>Polecane produkty z obliczoną gramaturą na posiłek</h3>
                        <h4 class="text-left eb-plan-header-makroname-top">Źródła białek</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <c:if test="${summary.wantRedMeat == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Czerwone mięso</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 1}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${summary.wantPoultry == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Drób</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 2}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${summary.wantDairyProducts == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Nabiał</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 3}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </div>
                            <div class="col-sm-6">
                                <c:if test="${summary.wantFish == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Ryby</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 4}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>

                                <c:if test="${summary.wantSeaFood == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Owoce morza</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 5}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${summary.wantLegume == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Warzywa strączkowe</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 6}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${summary.wantEggProducts == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Jajka</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 7}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${summary.wantNuts == 1}">
                                    <h4 class="text-left ml-2 eb-plan-header-makroname">Orzechy</h4>
                                    <c:forEach var="plan" items="${requestScope.plans}">
                                        <c:if test="${plan.planCategory == 8}">
                                            <div class="row">
                                                <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                    <c:out value="${plan.planName}"></c:out></div>
                                                <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                    <c:out value="${plan.planProtein}"></c:out> g
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </div>
                        <h4 class="text-left eb-plan-header-makroname-top">Źródła węglowodanów</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Kasze</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 11}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out>g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Ryż</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 12}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Płatki</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 13}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Mąki</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 14}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Pieczywo</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 15}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Makarony</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 17}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Bulwy</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 18}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Owoce</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 16}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planCarbo}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>
                        <h4 class="text-left eb-plan-header-makroname-top">Źródła tłuszczy</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Oleje roślinne</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 19}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planFat}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="text-left ml-2 eb-plan-header-makroname">Tłuszcze zwierzęce</h4>
                                <c:forEach var="plan" items="${requestScope.plans}">
                                    <c:if test="${plan.planCategory == 20}">
                                        <div class="row">
                                            <div class="col-8 d-block mx-auto text-uppercase text-left eb-plan-form-category">
                                                <c:out value="${plan.planName}"></c:out></div>
                                            <div class="col-4 d-block mx-auto text-left eb-plan-form-text">
                                                <c:out value="${plan.planFat}"></c:out> g
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="eb-plan-form-recipes">
                        <h3>Przykładowe przepisy</h3>
                        <c:set var="plan" value="${requestScope.plans}"/>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Jajecznica z warzywami</h4>
                            <p class="ml-3 mr-3 text-justify"> Pokrojoną czerwoną cebulę podsmażamy na maśle do
                                zeszklenia.
                                Następnie dodajemy pokrojonego pomidora. Waga warzyw nie mniej niż zgodnie z wzorem
                                wyżej może być więcej.
                                Wbijamy jaja kurze <c:out value="${plan.get(3).planProtein}"></c:out>g i lekko mieszając
                                smażymy
                                aż białka się zetną.
                                Jajecznice jemy z chlebem żytnim <c:out value="${plan.get(37).planCarbo}"></c:out>g.
                                Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Omlet z warzywami</h4>
                            <p class="ml-3 mr-3 text-justify"> Jajka <c:out value="${plan.get(3).planProtein}"></c:out>g
                                wraz z
                                odrobiną wody oraz mąką żytnią <c:out value="${plan.get(38).planCarbo}"></c:out>g
                                dokładnie ze sobą mieszamy
                                aż mieszanina będzie jednorodna i bez grudek. Cebulę, pomidorki koktajlowe,
                                paprykę kroimy w kostkę i podsamażamy lekko na patelni.
                                Waga warzyw nie mniej niż zgodnie z wzorem wyżej może być więcej.
                                Następnie wczesniej przygotowana masę wlewamy na patelnie.
                                Po kilku chwilach obracamy omlet na drugą stronę.
                                Zaleca się smażyć na średnim ogniu by wnętrze omletu też się ścięło.
                                Gotowy omlet posypujemy szczypiorkiem. Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Sałatka z gotowanym jajkiem</h4>
                            <p class="ml-3 mr-3 text-justify"> Jajka <c:out value="${plan.get(3).planProtein}"></c:out>g
                                gotujemy na twardo. Chleb pieczemy <c:out value="${plan.get(37).planCarbo}"></c:out>g w
                                tosterze i
                                kroimy w kostkę. Sałatę lodową szarpiemy na małe kawałki i dodajemy do niej oliwki bez
                                pestek, czerowoną paprykę pokrojoną w paski oraz rukolę. Waga warzyw nie mniej niż
                                zgodnie z wzorem wyżej może być więcej. Dodajemy wcześniej przygotowane
                                grzanki z chleba, dokładnie mieszamy sałatkę po dodaniu oliwy z oliwek. Do wymieszanej
                                sałatki dodajemy jajka pokrojone w ćwiartki. Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Pierś z kurczaka z ryżem i warzywami</h4>
                            <p class="ml-3 mr-3 text-justify"> Pierś z kurczaka <c:out
                                    value="${plan.get(0).planProtein}"></c:out>g kroimy w kostkę i przyprawiamy solą i
                                pieprzem. Wrzucamy kurczaka na rozgrzaną patelnie z olejem i smażymy około 4 minut. W
                                następnej kolejności dodajemy pokrojone warzywa: marchewkę, cukinię, paprykę i
                                doprawiamy: kurkumą, curry, słodką papryką. Dokładnie mieszamy i po chwili dodajemy
                                gotowany ryż <c:out value="${plan.get(32).planCarbo}"></c:out>g, cały czas mieszając.
                                Waga
                                warzyw nie mniej niż zgodnie z wzorem wyżej może
                                być więcej. Gdy kurczak będzie w pełni wysmażony a warzywa lekko
                                zmiękną można skończyć smażenie. Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Grillowana pierś z kurczaka</h4>
                            <p class="ml-3 mr-3 text-justify"> Filetujemy pierś z kurczaka <c:out
                                    value="${plan.get(0).planProtein}"></c:out>g na cieńkie plastry.
                                Grillujemy je na patelni grillowej lub w sezonie grillowym na ruszcie po wcześniejszym
                                przyprawieniu solą i pieprzem. Obrane ziemniaki <c:out
                                        value="${plan.get(42).planCarbo}"></c:out>g kroimy w grubszą kostkę i polewamy
                                odrobiną oleju oraz przyprawiamy ziołami. Ziemniaki pieczemy w piekarniku na papierze do
                                pieczenia w temp. 200 stopni do lekkiego zarumienienia się. Sałątę lodową szarpiemy na
                                małe kawałki i dodajemy rukolę, pokrojonego pomidora i pokrojonego ogórka zielonego.
                                Waga warzyw nie mniej niż
                                zgodnie z wzorem wyżej może być więcej.
                                Grillowaną pierś, pieczone ziemniaki i sałatkę przekładamy na talerz. Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Tortilla z kurczakiem</h4>
                            <p class="ml-3 mr-3 text-justify"> Kurczaka <c:out
                                    value="${plan.get(0).planProtein}"></c:out>g kroimy w drobną kostkę, doprawiamy i
                                smażymy
                                na
                                patelni. Następnie kurczaka przekładamy na placki tortilli. Do kurczaka dodajmy
                                pokrojoną paprykę, ogórka, pomidora i sałatę. Waga warzyw nie mniej niż
                                zgodnie z wzorem wyżej może być więcej. Placek zawijamy. Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Papryki faszerowane</h4>
                            <p class="ml-3 mr-3 text-justify"> Przyprawione solą i pieprzem mięso mielone z indyka
                                <c:out
                                        value="${plan.get(1).planProtein}"></c:out>g
                                wysmażmy dokładnie. Białą posiekaną cebulę oraz posiekane pieczarki podsmażamy na maśle
                                klarowanym. Waga warzyw nie mniej niż zgodnie z wzorem wyżej może być więcej. Paprykę
                                myjemy i wyjmujemy nasionaprzez odkrojony czubek z ogonkiem. Ryż <c:out
                                        value="${plan.get(33).planCarbo}"></c:out>g
                                gotujemy około 15 minut. Mięso, cebulę, pierczarki, ryż mieszamy na patelni i
                                faszerujemy nimi paprykę. Pieczemy w naczyniu żaroodpornym aż papryka lekko zwiędnie,
                                ale się nie przypali. Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Zapiekanka z bakłażana i cukinii</h4>
                            <p class="ml-3 mr-3 text-justify"> Przyprawione solą i pieprzem mięso mielone z indyka
                                <c:out
                                        value="${plan.get(1).planProtein}"></c:out>g smażymy jako
                                pierwsze, następnie dodajemy pokrojone w kostkę bakłażan i cukinie. Waga warzyw nie
                                mniej niż zgodnie z wzorem wyżej może być więcej. Można tutaj dodać
                                dużą łyżkę przecieru pomidorowego. Dopraw dodatkowo według swojego gustu. Warzywa smażyć
                                aż lekko zmiękną. Gotujemy makaron <c:out
                                        value="${plan.get(34).planCarbo}"></c:out>g do miękkości. Następnie przekładamy
                                odcedzony makaron
                                jak pierwszy do naczynia żaroodpornego i na niego mięso z warzywami i posypujemy serem
                                mozzarella (żółtym). Wkładamy do pieca rozgrzanego do 200stopni na 15 minut. Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Grillowana pierś z kurczaka z guacamole</h4>
                            <p class="ml-3 mr-3 text-justify"> Obrać bataty <c:out
                                    value="${plan.get(44).planCarbo}"></c:out>g i pokroić w kostkę. Polać łyżką oliwy z
                                oliwek i przyprawić ziołami. Piec w temp. 180 stopni aż się lekko zarumienią około 25-30
                                min na papierze do pieczenia. Pierś z kurczaka <c:out
                                        value="${plan.get(0).planProtein}"></c:out>g pokroić w cienkie filety oraz lekko
                                grillować na patelni grillowej lub w piekarniku. Po wstępnej obróbce dodać odrobinę
                                mozzarelli (żółtej) na wierzch (kurczak nadal jest grillowany) i poczekać aż ser się
                                roztopi. Przełożyć na talerz do pieczonych batatów. Następnie na ser położyć guacamole
                                (roztarte awokado z dodatkiem soku z limonki, soli, z dodatkiem kolendry, czosnku i
                                drobno pokrojonego pomidora). Gotowe!</p>
                        </div>
                        <div class="d-block mx-auto text-left eb-plan-form-category">
                            <h4>Burger</h4>
                            <p class="ml-3 mr-3 text-justify"> Mięso mielone z wołowiny <c:out
                                    value="${plan.get(13).planProtein}"></c:out>g przyprawić pieprzem i solą.
                                Mięso uformować w płaski kotlet smażyć około 8-10 min do pełnego wysmażenia(często
                                przewracać). Bułkę wieloziarnistą <c:out
                                        value="${plan.get(37).planCarbo}"></c:out>g przekrawamy na pół. Kładziemy na nią
                                sałatę, następnie
                                kotlety, pomidora, ogórka kiszonego. Możemy dodać musztardy oraz majonezu (tutaj jako
                                źródło dodatkowego tłuszczu). Gotowe!</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" col-4 text-center mx-auto mb-5">
                <button class="btn eb-generate-btn text-uppercase py-2 px-4" name="delete" value="1" type="submit">
                    Pobierz plan w pliku PDF</button>
            </div>
        </form>
    </div>
</section>

<!-- footer -->

<jsp:include page="fragment/footer.jspf"></jsp:include>


<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>
