<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- login -->
<section>
    <div class="col eb-signin text-center mb-5">
        <form class="col-sm-8 col-md-6 col-lg-4 mx-auto d-block eb-signin-form" method="post" action="j_security_check">
            <h1 class="mb-3 text-danger">Błąd logowania<br>spróbuj jeszcze raz</h1>
            <label class="sr-only mb-3">Nazwa użytkownika</label>
            <input name="j_username" type="text" class="form-control eb-signin-input mb-3"
                   placeholder="Nazwa użytkownika" required autofocus>
            <label class="sr-only mb-3">Hasło</label>
            <input name="j_password" type="password" class="form-control eb-signin-input" placeholder="Hasło" required>
            <div class="checkbox mb-3 mt-3">
                <label>
                    <input type="checkbox" value="remember-me"> Zapamiętaj hasło
                </label>
            </div>
            <button class="btn btn-lg btn-block eb-form-btn" type="submit">Zaloguj się</button>
            <a href="/register" class="btn btn-lg btn-block eb-form-btn mb-3">Zarejestruj się</a>
            <p class="mt-5 mb-3">© Eatbetter 2020</p>
        </form>
    </div>
</section>


<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>

