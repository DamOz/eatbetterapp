<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-10-27
  Time: 18:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

        <jsp:include page="fragment/navbar.jspf" />

<!-- summary -->
<section>
    <div class="container eb-generation">
        <form action="summary" method="post">
            <div class="eb-generation-header text-center mb-5">
                <c:set var="summary" value="${requestScope.summary}"> </c:set>
                <c:set var="converter" value="${requestScope.converter}"> </c:set>
                <h1 class="text-uppercase mb-3">Podałeś następujące informacje o sobie
                    <c:out value="${summary.user.username}"/> :</h1>
            </div>
            <div class="eb-generation-a mb-5">
                <div class="row">
                    <div class="col-xs-11 col-sm-10 col-md-8 col-lg-5 d-block mr-auto ml-auto mb-5 mb-lg-0 eb-generation-form">
                        <h3 class="text-center mb-3"> Informacje potrzebne do określenia kaloryczności oraz gramatury posiłków</h3>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Jaki jest Twój cel?</label>
                            <div class="col">
                                <div class="form-group">
                                    <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showGoal(summary.metricGoal)}"/></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col col-form-label eb-generation-form-text">Ile masz lat?</label>
                            <div class="col">
                                <div class="form-group">
                                    <p class="col-form-label eb-generation-form-text"><c:out value="${summary.metricAge}"/>  lat</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Jaka jest Twoja płeć?</label>
                            <div class="col">
                                <div class="form-group">
                                    <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showSex(summary.metricSex)}"/></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col col-form-label eb-generation-form-text">Jaka jest Twoja aktualna waga?</label>
                            <div class="col">
                                <div class="form-group">
                                    <p class="col-form-label eb-generation-form-text"><c:out value="${summary.metricWeight}"/>  kg</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col col-form-label eb-generation-form-text">Ile masz wzrostu?</label>
                            <div class="col">
                                <div class="form-group">
                                    <p class="col-form-label eb-generation-form-text"><c:out value="${summary.metricGrowth}"/>  cm</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col col-form-label eb-generation-form-text">Ile godzin w tygodniu trenujesz?</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showHours(summary.metricHours)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col col-form-label eb-generation-form-text">Jaki jest typ twojej pracy?</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showWork(summary.metricWork)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col col-form-label eb-generation-form-text">Jaki jest Twój typ budowy ciała?</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showBody(summary.metricBody)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col col-form-label eb-generation-form-text">Ile chciałbyś jeść posiłków dziennie?</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${summary.metricMeals}"/></p>
                            </div>
                        </div>
                    </div>

                    <!-- additional info -->

                    <div class="col-xs-11 col-sm-10 col-md-8 col-lg-5 d-block mr-auto ml-auto eb-generation-form">
                        <h3 class="text-center mt-3 mb-5">Twoje preferencje żywieniowe, czego
                            <span class="text-uppercase text-danger">nie chcesz</span> mieć w planie</h3>
                        <div class="form-group row ml-5 mb-4">
                            <label  class="col col-form-label eb-generation-form-text">Mięso czerwone</label>
                            <div class="col">
                               <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantRedMeat)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label  class="col col-form-label eb-generation-form-text">Drób</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantPoultry)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label  class="col col-form-label eb-generation-form-text">Nabiał</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantDairyProducts)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label  class="col col-form-label eb-generation-form-text">Jaja kurze</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantEggProducts)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Ryby</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantFish)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label  class="col col-form-label eb-generation-form-text">Owoce morza</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantSeaFood)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label  class="col col-form-label eb-generation-form-text">Warzywa strączkowe</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantLegume)}"/></p>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label  class="col col-form-label eb-generation-form-text">Orzechy</label>
                            <div class="col">
                                <p class="col-form-label eb-generation-form-text"><c:out value="${converter.showForeclosure(summary.wantNuts)}"/></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class=" col-4 text-center mx-auto mb-5">
                <button class="btn eb-generate-btn text-uppercase py-2 px-4" name="delete" value="0" type="submit">
                    Zatwierdź informacje
                </button>
            </div>
            <div class=" col-4 text-center mx-auto mb-5">
                <button class="btn eb-generate-btn text-uppercase py-2 px-4" name="delete" value="1" type="submit">Wprowadż informacje ponownie</button>
            </div>
        </form>
    </div>
</section>

<!-- footer -->

        <jsp:include page="fragment/footer.jspf" />


<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>
