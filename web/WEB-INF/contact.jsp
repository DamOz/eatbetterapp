<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-10-27
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- slideshow intro -->
<section>
    <header class="container eb-intro">
        <div class="eb-intro-slideshow">
            <img src="../resources/img/slideshow/salad.jpg">
            <img src="../resources/img/slideshow/salmon.jpg">
            <img src="../resources/img/slideshow/steak.jpg">
        </div>
        <div class="eb-intro-header">
            <h1>Eatbetter</h1>
            <p>Jedz lepiej dla lepszego samopoczucia</p>
        </div>
    </header>
</section>

<!-- contact -->

<div class="container-fluid eb-contact">
    <div class="row eb-contact-upperbox">
        <div class="col-lg-8 eb-contact-mainbox">
            <div class="eb-contact-overlay mx-auto">
                <h1>Chcesz się skontaktować?</h1>
                <h2>Chętnie wysłucham, co masz mi do przkazania lub zaproponowania.</h2>
                <h3>Dane kontaktowe</h3>
                <h2>Adres e-mail: damian@damianozga.pl</h2>
                <a href="#"> <i class="icon-facebook-squared eb-contact-icon"></i></a>

                <h2 class="mt-4 eb-contact-bottom-text">Proszę być cierpliwym. Nie zawsze mam możliwość odpowiedzieć
                    od razu. Jeśli Ci nie odpowiedziałem patrz w prawo.</h2>
            </div>
        </div>
        <div class="col-lg-4 eb-contact-mainbox-2">
            <h1>Przypominam</h1>
            <h2 class="eb-contact-mainbox-2-text text-justify">Obecnie projekt jest nadal rozwijany. W miarę możliwości
                będę go uaktualniał. Jeśli zauważysz jakieś literówki lub inne błędy proszę powiadom mnie o nich.</h2>
            <h2 class="eb-contact-mainbox-2-text text-justify"> Na wiadomości świadczące o <span
                    class="eb-contact-mainbox-2-text-span"> niskiej </span>kulturze osobistej
                <span class="eb-contact-mainbox-2-text-span"> nadawcy </span>nie będę odpowiadał.</h2>
        </div>
    </div>
</div>
</div>

</
>

<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>
