<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-11-10
  Time: 16:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- new recipe form -->

<section>
    <div class="col eb-signin text-center mb-5">
        <form class="col-sm-12 col-md-8 col-lg-6 mx-auto d-block eb-signin-form" action="new-product" method="post">
            <h1 class="mb-3">Śmiało, dodaj nowy product!</h1>
            <label class="sr-only mb-3 ">Nazwa produktu</label>
            <input placeholder="Nazwa" type="text" name="inputProductName" class="form-control eb-signin-input mb-3"
                   required autofocus>
            <label class="sr-only mb-3 ">Ilość g białka</label>
            <input placeholder="białko" type="number" name="inputProductProtein"
                   class="form-control eb-signin-input mb-3" required autofocus>
            <label class="sr-only mb-3 ">Ilość g węglowodanów</label>
            <input placeholder="węglowodany" type="number" name="inputProductCarbo"
                   class="form-control eb-signin-input mb-3" required autofocus>
            <label class="sr-only mb-3 ">Ilość g tłuszczu</label>
            <input placeholder="tłuszcze" type="number" name="inputProductFat" class="form-control eb-signin-input mb-3"
                   required autofocus>
            <label class="sr-only mb-3 ">Ilość g błonnika pokarmowego</label>
            <input placeholder="błonnik pokarmowy" type="number" name="inputProductFiber"
                   class="form-control eb-signin-input mb-3" required autofocus>
            <label class="sr-only mb-3 ">Kategoria produktu</label>
            <input placeholder="categoria produktu" type="number" max="20" name="inputProductCategory"
                   class="form-control eb-signin-input mb-3" required autofocus>

            <button class="btn btn-lg btn-block eb-form-btn" type="submit">Dodaj przepis</button>
        </form>
    </div>
</section>


<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>
</title>
</head>
<body>

</body>
</html>
