package pl.damianozga.eatbetter.pdf;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import pl.damianozga.eatbetter.model.Calories;
import pl.damianozga.eatbetter.model.Metric;
import pl.damianozga.eatbetter.model.Plan;
import pl.damianozga.eatbetter.model.User;
import pl.damianozga.eatbetter.service.CaloriesService;
import pl.damianozga.eatbetter.service.MetricService;
import pl.damianozga.eatbetter.service.PlanService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PdfPlanCreator {

    public byte[] createPlan(HttpServletRequest request) throws Exception {
        // connecting to db to get calories object
        User tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        MetricService metricService = new MetricService();
        Metric tempMetric = metricService.getMetricById(user_id);
        long metric_id = tempMetric.getMetricId();
        CaloriesService caloriesService = new CaloriesService();
        Calories tempCalories = caloriesService.getCaloriesById(metric_id);
        long calories_id = tempCalories.getCaloriesId();
        PlanService planService = new PlanService();
        java.util.List<Plan> plan = planService.getPlan(calories_id);

        // Writing pdf file
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter writer = new PdfWriter(baos);



        // Setting font and encoding
        String encodings = PdfEncodings.CP1250;
        PdfFont fontHelvetica = PdfFontFactory.createFont(StandardFonts.HELVETICA, encodings);
        PdfFont fontHelveticaBold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD, encodings);


        // Creating a PdfDocument object
        PdfDocument pdfDoc = new PdfDocument(writer);

        //adding a empty page
        pdfDoc.addNewPage();

        //Creating the document
        Document document = new Document(pdfDoc);

        //Integer elements for the meal option
        int meal3 = 3;
        int meal4 = 4;
        int meal5 = 5;
        int wanted = 1;
        final double firstMeal3 = 0.2;
        double firstMeal3Vegetables = tempCalories.getCaloriesVegetables() * firstMeal3;
        final double secondMeal3 = 0.5;
        double secondMeal3Vegetables = tempCalories.getCaloriesVegetables() * secondMeal3;
        final double thirdMeal3 = 0.3;
        double thirdMeal3Vegetables = tempCalories.getCaloriesVegetables() * thirdMeal3;
        final double firstMeal4 = 0.15;
        double firstMeal4Vegetables = tempCalories.getCaloriesVegetables() * firstMeal4;
        final double secondMeal4 = 0.2;
        double secondMeal4Vegetables = tempCalories.getCaloriesVegetables() * secondMeal4;
        final double thirdMeal4 = 0.4;
        double thirdMeal4Vegetables = tempCalories.getCaloriesVegetables() * thirdMeal4;
        final double fourthMeal4 = 0.25;
        double fourthMeal4Vegetables = tempCalories.getCaloriesVegetables() * fourthMeal4;
        final double firstMeal5 = 0.1;
        double firstMeal5Vegetables = tempCalories.getCaloriesVegetables() * firstMeal5;
        final double secondMeal5 = 0.2;
        double secondMeal5Vegetables = tempCalories.getCaloriesVegetables() * secondMeal5;
        final double thirdMeal5 = 0.3;
        double thirdMeal5Vegetables = tempCalories.getCaloriesVegetables() * thirdMeal5;
        final double fourthMeal5 = 0.3;
        double fourthMeal5Vegetables = tempCalories.getCaloriesVegetables() * fourthMeal5;
        final double fifthMeal5 = 0.1;
        double fifthMeal5Vegetables = tempCalories.getCaloriesVegetables() * fifthMeal5;

        //String element for the plan
        String header = "PLAN ŻYWIENIOWY WYGENEROWANY DLA";
        String headerUserName = tempUser.getUsername().toUpperCase();


        String caloricDemand = "KALORYCZNOŚĆ I GRAMATURA";
        String dCaloricDemand = "TWOJE DZIENNE ZAPOTRZEBOWANIE KALORYCZNE PODANE W KCAL: " + tempCalories.getCaloriesTotal() + " " + "Kcal";
        String dWaterDemand = "TWOJE DZIENNE ZAPOTRZEBOWANIE NA WODĘ PODANE W LITRACH TO: " + tempCalories.getCaloriesWater() + " " + "litra";
        String dProteinDemand = "ILOŚĆ GRAMÓW BIAŁKA NA POSIŁEK TO: " + tempCalories.getCaloriesMealProtein() + " " + "gram";
        String dCarboDemand = "ILOŚĆ GRAMÓW WĘGLOWODANÓW NA POSIŁEK TO: " + tempCalories.getCaloriesMealCarbo() + " " + "gram";
        String dFatDemand = "ILOŚĆ GRAMÓW TŁUSZCZY NA POSIŁEK TO: " + tempCalories.getCaloriesMealFat() + " " + "gram";
        String mealHeader = "OGÓNY WZÓR NA POSIŁEK PRZY SAMODZIELNYM KOMPONOWANIU";
        String vegetables = " gram warzyw";
        String mealI = "Posiłek I: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + firstMeal3Vegetables + vegetables;
        String mealII = "Posiłek II: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + secondMeal3Vegetables + vegetables;
        String mealIII = "Posiłek III: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + thirdMeal3Vegetables + vegetables;
        String mealI4 = "Posiłek I: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + firstMeal4Vegetables + vegetables;
        String mealII4 = "Posiłek II: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + secondMeal4Vegetables + vegetables;
        String mealIII4 = "Posiłek III: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + thirdMeal4Vegetables + vegetables;
        String mealIV4 = "Posiłek IV: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + fourthMeal4Vegetables + vegetables;
        String mealI5 = "Posiłek I: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + firstMeal5Vegetables + vegetables;
        String mealII5 = "Posiłek II: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + secondMeal5Vegetables + vegetables;
        String mealIII5 = "Posiłek III: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + thirdMeal5Vegetables + vegetables;
        String mealIV5 = "Posiłek IV: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + fourthMeal5Vegetables + vegetables;
        String mealV5 = "Posiłek V: Porcja białka + porcja węglowodanów + porcja tłuszczy + " + fifthMeal5Vegetables + vegetables;

        String sourceOfProtein = "POLECANE ŹRÓDŁA BIAŁKA";
        String sourceOfProteinRedMeat = "Czerwone mięso:";
        String sourceOfProteinPoultry = "Drób:";
        String sourceOfProteinDairyProducts = "Nabiał:";
        String sourceOfProteinFish = "Ryby:";
        String sourceOfProteinSeaFood = "Owoce morza:";
        String sourceOfProteinLegume = "Warzywa strączkowe:";
        String sourceOfProteinEggs = "Jajka:";
        String sourceOfProteinNuts = "Orzechy:";

        String sourceOfCarbo = "POLECANE ŹRÓDŁA WĘGLOWODANÓW";
        String sourceOfCarboGroats = "Kasze";
        String sourceOfCarboRice = "Ryż";
        String sourceOfCarboFlakes = "Płatki";
        String sourceOfCarboFlour = "Mąki";
        String sourceOfCarboBread = "Pieczywo";
        String sourceOfCarboPastas = "Makarony";
        String sourceOfCarboTubers = "Bulwy";
        String sourceOfCarboOthers = "Owoce";

        String sourceOfFat = "POLECANE ŹRÓDŁA TŁUSZCZY";
        String sourceOfFatVegetablesOils = "Tłuszcze roślinne";
        String sourceOfFatAnimalFats = "Tłuszcze zwierzęce";

        String sourceOfVegetables = "POLECANE WARZYWA";
        String vegetablesExemples = "Bakłażan, boćwina, brokuły, brukselka, burak, cebula, cukinia, cykoria, dynia," +
                "fasola szparagowa, jarmuż, kalafior, kalarepa, kapusta biała, kapusta czerwona, kapusta pekińska," +
                "kapusta włoska, koper ogrodowy, marchew, ogórek, papryka czerwona, papryka zielona, pietruszka korzeń," +
                " pomidor, por, rabarbar, rzepa, rzodkiewka, sałata, seler, szczaw, szczypiorek, szparagi, szpinak.";

        String recipeHeader = "PRZYKŁADOWE PRZEPISY";
        String recipeName1 = "Jajecznica z warzywami";
        String recipeContent1 = "Pokrojoną czerwoną cebulę podsmażamy na maśle do zeszklenia. Następnie dodajemy " +
                "pokrojonego pomidora. Waga warzyw nie mniej niż zgodnie z wzorem wyżej, może być więcej. Wbijamy " +
                "jaja kurze " + plan.get(3).getPlanProtein() + "g i lekko mieszając smażymy aż białka się zetną. " +
                "Jajecznice jemy z chlebem żytnim " + plan.get(37).getPlanCarbo() + " g. Gotowe!";
        String recipeName2 = "Omlet z warzywami";
        String recipeContent2 = "Jajka " + plan.get(3).getPlanProtein() + "g wraz z odrobiną wody oraz mąką żytnią  "
                + plan.get(38).getPlanCarbo() + "g dokładnie ze sobą mieszamy aż mieszanina będzie jednorodna " +
                "i bez grudek. Cebulę, pomidorki koktajlowe, paprykę kroimy w kostkę i podsamażamy lekko na patelni." +
                " Waga warzyw nie mniej niż zgodnie z wzorem wyżej może być więcej. Następnie wczesniej przygotowana" +
                " masę wlewamy na patelnie. Po kilku chwilach obracamy omlet na drugą stronę. Zaleca się smażyć na " +
                "średnim ogniu by wnętrze omletu też się ścięło. Gotowy omlet posypujemy szczypiorkiem. Gotowe!";
        String recipeName3 = "Sałatka z gotowanym jajkiem";
        String recipeContent3 = "Jajka " + plan.get(3).getPlanProtein() + "g gotujemy na twardo. Chleb pieczemy  "
                + plan.get(37).getPlanCarbo() + "g w tosterze i kroimy w kostkę. Sałatę lodową szarpiemy na " +
                "małe kawałki i dodajemy do niej oliwki bez pestek, czerowoną paprykę pokrojoną w paski oraz rukolę. " +
                "Waga warzyw nie mniej niż zgodnie z wzorem wyżej może być więcej. Dodajemy wcześniej przygotowane" +
                " grzanki z chleba, dokładnie mieszamy sałatkę po dodaniu oliwy z oliwek. Do wymieszanej sałatki " +
                "dodajemy jajka pokrojone w ćwiartki. Gotowe!";
        String recipeName4 = "Pierś z kurczaka z ryżem i warzywami";
        String recipeContent4 = "Pierś z kurczaka " + plan.get(0).getPlanProtein() + "g kroimy w kostkę i przyprawiamy " +
                "solą i pieprzem. Wrzucamy kurczaka na rozgrzaną patelnie z olejem i smażymy około 4 minut." +
                " W następnej kolejności dodajemy pokrojone warzywa: marchewkę, cukinię, paprykę i doprawiamy: kurkumą," +
                " curry, słodką papryką. Dokładnie mieszamy i po chwili dodajemy gotowany ryż "
                + plan.get(32).getPlanCarbo() + "g, cały czas mieszając. Waga warzyw nie mniej niż zgodnie z wzorem" +
                " wyżej może być więcej. Gdy kurczak będzie w pełni wysmażony a warzywa lekko zmiękną można skończyć" +
                " smażenie. Gotowe!";
        String recipeName5 = "Grillowana pierś z kurczaka";
        String recipeContent5 = "Filetujemy pierś z kurczaka " + plan.get(0).getPlanProtein() + "g na cieńkie plastry. " +
                "Grillujemy je na patelni grillowej lub w sezonie grillowym na ruszcie po wcześniejszym przyprawieniu " +
                "solą i pieprzem. Obrane ziemniaki " + plan.get(42).getPlanCarbo() + "g kroimy w grubszą kostkę i " +
                "polewamy odrobiną oleju oraz przyprawiamy ziołami. Ziemniaki pieczemy w piekarniku na papierze do " +
                "pieczenia w temp. 200 stopni do lekkiego zarumienienia się. Sałątę lodową szarpiemy na małe kawałki " +
                "i dodajemy rukolę, pokrojonego pomidora i pokrojonego ogórka zielonego. Waga warzyw nie mniej niż " +
                "zgodnie z wzorem wyżej może być więcej. Grillowaną pierś, pieczone ziemniaki i sałatkę przekładamy " +
                "na talerz. Gotowe!";
        String recipeName6 = "Tortilla z kurczakiem";
        String recipeContent6 = "Kurczaka " + plan.get(0).getPlanProtein() + "g kroimy w drobną kostkę, doprawiamy " +
                "i smażymy na patelni. Następnie kurczaka przekładamy na placki tortilli. Do kurczaka dodajmy " +
                "pokrojoną paprykę, ogórka, pomidora i sałatę. Waga warzyw nie mniej niż zgodnie z wzorem wyżej " +
                "może być więcej. Placek zawijamy. Gotowe!";
        String recipeName7 = "Papryki faszerowane";
        String recipeContent7 = "Przyprawione solą i pieprzem mięso mielone z indyka " + plan.get(1).getPlanProtein() +
                "g wysmażmy dokładnie. Białą posiekaną cebulę oraz posiekane pieczarki podsmażamy na maśle klarowanym." +
                " Waga warzyw nie mniej niż zgodnie z wzorem wyżej może być więcej. Paprykę myjemy i wyjmujemy " +
                "nasionaprzez odkrojony czubek z ogonkiem. Ryż " + plan.get(33).getPlanCarbo() + "g gotujemy około " +
                "15 minut. Mięso, cebulę, pierczarki, ryż mieszamy na patelni i faszerujemy nimi paprykę. Pieczemy w " +
                "naczyniu żaroodpornym aż papryka lekko zwiędnie, ale się nie przypali. Gotowe!";
        String recipeName8 = "Zapiekanka z bakłażana i cukinii";
        String recipeContent8 = "Przyprawione solą i pieprzem mięso mielone z indyka " + plan.get(1).getPlanProtein() +
                "g smażymy jako pierwsze, następnie dodajemy pokrojone w kostkę bakłażan i cukinie. Waga warzyw nie " +
                "mniej niż zgodnie z wzorem wyżej może być więcej. Można tutaj dodać dużą łyżkę przecieru pomidorowego." +
                " Dopraw dodatkowo według swojego gustu. Warzywa smażyć aż lekko zmiękną. Gotujemy makaron "
                + plan.get(34).getPlanCarbo() + "g do miękkości. Następnie przekładamy odcedzony makaron jak pierwszy " +
                "do naczynia żaroodpornego i na niego mięso z warzywami i posypujemy serem mozzarella (żółtym). " +
                "Wkładamy do pieca rozgrzanego do 200stopni na 15 minut. Gotowe!";
        String recipeName9 = "Grillowana pierś z kurczaka z guacamole";
        String recipeContent9 = "Obrać bataty " + plan.get(44).getPlanCarbo() + "g i pokroić w kostkę. Polać łyżką " +
                "oliwy z oliwek i przyprawić ziołami. Piec w temp. 180 stopni aż się lekko zarumienią około 25-30 min " +
                "na papierze do pieczenia. Pierś z kurczaka " + plan.get(0).getPlanProtein() + "g pokroić w cienkie " +
                "filety oraz lekko grillować na patelni grillowej lub w piekarniku. Po wstępnej obróbce dodać odrobinę " +
                "mozzarelli (żółtej) na wierzch (kurczak nadal jest grillowany) i poczekać aż ser się roztopi. " +
                "Przełożyć na talerz do pieczonych batatów. Następnie na ser położyć guacamole (roztarte awokado " +
                "z dodatkiem soku z limonki, soli, z dodatkiem kolendry, czosnku i drobno pokrojonego pomidora). Gotowe!";
        String recipeName10 = "Burger";
        String recipeContent10 = "Mięso mielone z wołowiny " + plan.get(13).getPlanProtein() + "g przyprawić pieprzem i " +
                "solą. Mięso uformować w płaski kotlet smażyć około 8-10 min do pełnego wysmażenia(często przewracać). " +
                "Bułkę wieloziarnistą " + plan.get(37).getPlanCarbo() + "g przekrawamy na pół. Kładziemy na nią sałatę," +
                " następnie kotlety, pomidora, ogórka kiszonego. Możemy dodać musztardy oraz majonezu (tutaj jako " +
                "źródło dodatkowego tłuszczu). Gotowe!";


        // Creating the list of protein products
        List listOfRedMeat = new List();
        List listOfPoultry = new List();
        List listOfDairyProducts = new List();
        List listOfFish = new List();
        List listOfSeaFood = new List();
        List listOfLegume = new List();
        List listOfEggs = new List();
        List listOfNuts = new List();

        //Creating the list of carbo products
        List listOfGroats = new List();
        List listOfRice = new List();
        List listOfFlakes = new List();
        List listOfFlour = new List();
        List listOfBread = new List();
        List listOfPastas = new List();
        List listOfTubers = new List();
        List listOfOthers = new List();

        //Creating the list of fat products
        List listOfVegetablesOils = new List();
        List listOfAnimalFats = new List();

        //Add elements to the listOfProteins
        // RedMeat
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 1;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfRedMeat.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        // Poultry
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 2;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfPoultry.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        // DairyProducts
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 3;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfDairyProducts.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        // Fish
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 4;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfFish.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        // SeaFood
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 5;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfSeaFood.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        // Legume
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 6;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfLegume.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        // Eggs
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 7;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfEggs.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        // Nuts
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 8;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfNuts.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanProtein());
            }
        }

        //Add elements to the listOfCarbo
        // Groats
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 11;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfGroats.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }
        // Rice
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 12;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfRice.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }

        // Flakes
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 13;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfFlakes.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }

        // Flour
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 14;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfFlour.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }

        // Bread
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 15;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfBread.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }

        // Pastas
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 17;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfPastas.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }

        // Tubers
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 18;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfTubers.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }

        // Others
        for (int i = 0; i < plan.size(); i++) {
            int carboCategory = 16;
            if (plan.get(i).getPlanCategory() == carboCategory) {
                listOfOthers.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanCarbo());
            }
        }

        //Add elements to the listOfFats
        // Vegetables oils
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 19;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfVegetablesOils.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanFat());
            }
        }

        // Animal fats
        for (int i = 0; i < plan.size(); i++) {
            int proteinCategory = 20;
            if (plan.get(i).getPlanCategory() == proteinCategory) {
                listOfAnimalFats.add(plan.get(i).getPlanName() + " " + plan.get(i).getPlanFat());
            }
        }

        //Creating an Area Break object
        AreaBreak aB = new AreaBreak();

        //Creating an Empty Paragraph


        //Creating an paragraphs
        //Header
        Paragraph paragraphHeader = new Paragraph(header);
        Paragraph paragraphHeaderUserName = new Paragraph(headerUserName);
        //Caloric Demand
        Paragraph paragraphCaloricDemand = new Paragraph(caloricDemand);
        Paragraph paragraphDCaloricDemand = new Paragraph(dCaloricDemand);
        Paragraph paragraphDWaterDemand = new Paragraph(dWaterDemand);
        Paragraph paragraphDProteinDemand = new Paragraph(dProteinDemand);
        Paragraph paragraphDCarboDemand = new Paragraph(dCarboDemand);
        Paragraph paragraphDFatDemand = new Paragraph(dFatDemand);
        //Meals
        Paragraph paragraphMealHeader = new Paragraph(mealHeader);
        Paragraph paragraphMealI = new Paragraph(mealI);
        Paragraph paragraphMealII = new Paragraph(mealII);
        Paragraph paragraphMealIII = new Paragraph(mealIII);
        Paragraph paragraphMealI4 = new Paragraph(mealI4);
        Paragraph paragraphMealII4 = new Paragraph(mealII4);
        Paragraph paragraphMealIII4 = new Paragraph(mealIII4);
        Paragraph paragraphMealIV4 = new Paragraph(mealIV4);
        Paragraph paragraphMealI5 = new Paragraph(mealI5);
        Paragraph paragraphMealII5 = new Paragraph(mealII5);
        Paragraph paragraphMealIII5 = new Paragraph(mealIII5);
        Paragraph paragraphMealIV5 = new Paragraph(mealIV5);
        Paragraph paragraphMealV5 = new Paragraph(mealV5);
        //Proteins
        Paragraph paragraphSourceOfProteinRedMeat = new Paragraph(sourceOfProteinRedMeat);
        Paragraph paragraphSourceOfProteinPoultry = new Paragraph(sourceOfProteinPoultry);
        Paragraph paragraphSourceOfProteinDairyProducts = new Paragraph(sourceOfProteinDairyProducts);
        Paragraph paragraphSourceOfProteinFish = new Paragraph(sourceOfProteinFish);
        Paragraph paragraphSourceOfProteinSeaFood = new Paragraph(sourceOfProteinSeaFood);
        Paragraph paragraphSourceOfProteinLegume = new Paragraph(sourceOfProteinLegume);
        Paragraph paragraphSourceOfProteinEggs = new Paragraph(sourceOfProteinEggs);
        Paragraph paragraphSourceOfProteinNuts = new Paragraph(sourceOfProteinNuts);
        //Carbo
        Paragraph paragraphSourceOfCarboGroats = new Paragraph(sourceOfCarboGroats);
        Paragraph paragraphSourceOfCarboRice = new Paragraph(sourceOfCarboRice);
        Paragraph paragraphSourceOfCarboFlakes = new Paragraph(sourceOfCarboFlakes);
        Paragraph paragraphSourceOfCarboFlour = new Paragraph(sourceOfCarboFlour);
        Paragraph paragraphSourceOfCarboBread = new Paragraph(sourceOfCarboBread);
        Paragraph paragraphSourceOfCarboPastas = new Paragraph(sourceOfCarboPastas);
        Paragraph paragraphSourceOfCarboTubers = new Paragraph(sourceOfCarboTubers);
        Paragraph paragraphSourceOfCarboOther = new Paragraph(sourceOfCarboOthers);
        //Fats
        Paragraph paragraphSourceOfFatVegetablesOils = new Paragraph(sourceOfFatVegetablesOils);
        Paragraph paragraphSourceOfFatAnimalFats = new Paragraph(sourceOfFatAnimalFats);

        //Vegetables
        Paragraph paragraphSourceOfVegetables = new Paragraph(sourceOfVegetables);
        Paragraph paragraphVegetablesExemples = new Paragraph(vegetablesExemples);

        // headers
        Paragraph paragraphSourceOfProtein = new Paragraph(sourceOfProtein).setFont(fontHelvetica);
        Paragraph paragraphsourceOfCarbo = new Paragraph(sourceOfCarbo).setFont(fontHelvetica);
        Paragraph paragraphsourceOfFat = new Paragraph(sourceOfFat).setFont(fontHelvetica);


        Paragraph paragraphRecipeHeader = new Paragraph(recipeHeader).setFont(fontHelvetica);
        Paragraph paragraphRecipeName1 = new Paragraph(recipeName1).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent1 = new Paragraph(recipeContent1).setFont(fontHelvetica);
        Paragraph paragraphRecipeName2 = new Paragraph(recipeName2).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent2 = new Paragraph(recipeContent2).setFont(fontHelvetica);
        Paragraph paragraphRecipeName3 = new Paragraph(recipeName3).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent3 = new Paragraph(recipeContent3).setFont(fontHelvetica);
        Paragraph paragraphRecipeName4 = new Paragraph(recipeName4).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent4 = new Paragraph(recipeContent4).setFont(fontHelvetica);
        Paragraph paragraphRecipeName5 = new Paragraph(recipeName5).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent5 = new Paragraph(recipeContent5).setFont(fontHelvetica);
        Paragraph paragraphRecipeName6 = new Paragraph(recipeName6).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent6 = new Paragraph(recipeContent6).setFont(fontHelvetica);
        Paragraph paragraphRecipeName7 = new Paragraph(recipeName7).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent7 = new Paragraph(recipeContent7).setFont(fontHelvetica);
        Paragraph paragraphRecipeName8 = new Paragraph(recipeName8).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent8 = new Paragraph(recipeContent8).setFont(fontHelvetica);
        Paragraph paragraphRecipeName9 = new Paragraph(recipeName9).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent9 = new Paragraph(recipeContent9).setFont(fontHelvetica);
        Paragraph paragraphRecipeName10 = new Paragraph(recipeName10).setFont(fontHelvetica);
        Paragraph paragraphRecipeContent10 = new Paragraph(recipeContent10).setFont(fontHelvetica);


        // Adding components to the PDF plan
        // Header
        document.add(paragraphHeader.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER).setFontSize(16));
        document.add(new Paragraph());
        document.add(paragraphHeaderUserName.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER).setFontSize(14));
        document.add(new Paragraph());
        document.add(new Paragraph());
        // Caloric Demand
        document.add(paragraphCaloricDemand.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER).setFontSize(14));
        document.add(new Paragraph());
        document.add(paragraphDCaloricDemand.setFont(fontHelvetica));
        document.add(paragraphDWaterDemand.setFont(fontHelvetica));
        document.add(paragraphDProteinDemand.setFont(fontHelvetica));
        document.add(paragraphDCarboDemand.setFont(fontHelvetica));
        document.add(paragraphDFatDemand.setFont(fontHelvetica));
        document.add(new Paragraph());
        document.add(new Paragraph());
        // Meals
        document.add(paragraphMealHeader.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER).setFontSize(14));
        document.add(new Paragraph());
        if (tempMetric.getMetricMeals() == meal3) {
            document.add(paragraphMealI.setFont(fontHelvetica));
            document.add(paragraphMealII.setFont(fontHelvetica));
            document.add(paragraphMealIII.setFont(fontHelvetica));
        }
        if (tempMetric.getMetricMeals() == meal4) {
            document.add(paragraphMealI4.setFont(fontHelvetica));
            document.add(paragraphMealII4.setFont(fontHelvetica));
            document.add(paragraphMealIII4.setFont(fontHelvetica));
            document.add(paragraphMealIV4.setFont(fontHelvetica));
        }
        if (tempMetric.getMetricMeals() == meal5) {
            document.add(paragraphMealI5.setFont(fontHelvetica));
            document.add(paragraphMealII5.setFont(fontHelvetica));
            document.add(paragraphMealIII5.setFont(fontHelvetica));
            document.add(paragraphMealIV5.setFont(fontHelvetica));
            document.add(paragraphMealV5.setFont(fontHelvetica));
        }
        document.add(new Paragraph());
        document.add(new Paragraph());

        // Sources of the proteins
        document.add(paragraphSourceOfProtein.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER).setFontSize(14));
        document.add(new Paragraph());
        if (tempMetric.getWantRedMeat() == wanted) {
            document.add(paragraphSourceOfProteinRedMeat.setFont(fontHelveticaBold));
            document.add(listOfRedMeat.setFont(fontHelvetica));
        }
        if (tempMetric.getWantPoultry() == wanted) {
            document.add(paragraphSourceOfProteinPoultry.setFont(fontHelveticaBold));
            document.add(listOfPoultry.setFont(fontHelvetica));
        }
        if (tempMetric.getWantDairyProducts() == wanted) {
            document.add(paragraphSourceOfProteinDairyProducts.setFont(fontHelveticaBold));
            document.add(listOfDairyProducts.setFont(fontHelvetica));
        }
        if (tempMetric.getWantFish() == wanted) {
            document.add(paragraphSourceOfProteinFish.setFont(fontHelveticaBold));
            document.add(listOfFish.setFont(fontHelvetica));
        }
        if (tempMetric.getWantSeaFood() == wanted) {
            document.add(paragraphSourceOfProteinSeaFood.setFont(fontHelveticaBold));
            document.add(listOfSeaFood.setFont(fontHelvetica));
        }
        if (tempMetric.getWantLegume() == wanted) {
            document.add(paragraphSourceOfProteinLegume.setFont(fontHelveticaBold));
            document.add(listOfLegume.setFont(fontHelvetica));
        }
        if (tempMetric.getWantEggProducts() == wanted) {
            document.add(paragraphSourceOfProteinEggs.setFont(fontHelveticaBold));
            document.add(listOfEggs.setFont(fontHelvetica));
        }
        if (tempMetric.getWantNuts() == wanted) {
            document.add(paragraphSourceOfProteinNuts.setFont(fontHelveticaBold));
            document.add(listOfNuts.setFont(fontHelvetica));
        }
        document.add(new Paragraph());
        document.add(new Paragraph());

        //Sources of the carbo
        document.add(paragraphsourceOfCarbo.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER).setFontSize(14));
        document.add(new Paragraph());
        document.add(paragraphSourceOfCarboGroats.setFont(fontHelveticaBold));
        document.add(listOfGroats.setFont(fontHelvetica));

        document.add(paragraphSourceOfCarboRice.setFont(fontHelveticaBold));
        document.add(listOfRice.setFont(fontHelvetica));

        document.add(paragraphSourceOfCarboFlakes.setFont(fontHelveticaBold));
        document.add(listOfFlakes.setFont(fontHelvetica));

        document.add(paragraphSourceOfCarboFlour.setFont(fontHelveticaBold));
        document.add(listOfFlour.setFont(fontHelvetica));

        document.add(paragraphSourceOfCarboBread.setFont(fontHelveticaBold));
        document.add(listOfBread.setFont(fontHelvetica));

        document.add(paragraphSourceOfCarboPastas.setFont(fontHelveticaBold));
        document.add(listOfPastas.setFont(fontHelvetica));

        document.add(paragraphSourceOfCarboTubers.setFont(fontHelveticaBold));
        document.add(listOfTubers.setFont(fontHelvetica));

        document.add(paragraphSourceOfCarboOther.setFont(fontHelveticaBold));
        document.add(listOfOthers.setFont(fontHelvetica));
        document.add(new Paragraph());
        document.add(new Paragraph());

        //Sources of fat
        document.add(paragraphsourceOfFat.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER).setFontSize(14));
        document.add(new Paragraph());
        document.add(paragraphSourceOfFatVegetablesOils.setFont(fontHelveticaBold));
        document.add(listOfVegetablesOils.setFont(fontHelvetica));

        document.add(paragraphSourceOfFatAnimalFats.setFont(fontHelveticaBold));
        document.add(listOfAnimalFats.setFont(fontHelvetica));

        //Vegetables
        document.add(paragraphSourceOfVegetables.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER));
        document.add(new Paragraph());
        document.add(paragraphVegetablesExemples.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        //Recipes
        document.add(paragraphRecipeHeader.setFont(fontHelveticaBold).setTextAlignment(TextAlignment.CENTER));
        document.add(new Paragraph());
        document.add(paragraphRecipeName1.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent1.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName2.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent2.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName3.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent3.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName4.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent4.setFont(fontHelvetica));

        document.add(paragraphRecipeName5.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent5.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName6.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent6.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName7.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent7.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName8.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent8.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName9.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent9.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        document.add(paragraphRecipeName10.setFont(fontHelveticaBold).setFontSize(13));
        document.add(paragraphRecipeContent10.setFont(fontHelvetica).setTextAlignment(TextAlignment.JUSTIFIED));

        //Closing the document
        document.close();

        // giving as byte[]
        byte[] bytes = baos.toByteArray();

        try{
            baos.close();
        } catch (IOException e) {}
        return bytes;

    }

    public void getPlan(HttpServletRequest request, HttpServletResponse response, byte[] bytes) throws ServletException, IOException {

        // sending a pdf plan to the user
        response.reset();
        String pdfFileName = "testPlan.pdf";
        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "attachment; filename=" + pdfFileName);
        OutputStream responseOutputStream = response.getOutputStream();
        responseOutputStream.write(bytes);
        responseOutputStream.close();



    }
}
