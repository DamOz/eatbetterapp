package pl.damianozga.eatbetter.model;

import java.util.Objects;

public class Metric {
    private long metricId;
    private int metricGoal;
    private int metricAge;
    private int metricSex;
    private int metricWeight;
    private int metricGrowth;
    private int metricHours;
    private int metricWork;
    private int metricBody;
    private int metricMeals;
    private int wantRedMeat;
    private int wantPoultry;
    private int wantDairyProducts;
    private int wantEggProducts;
    private int wantFish;
    private int wantSeaFood;
    private int wantLegume;
    private int wantNuts;
    private User user;

    public Metric() {
    }

    public Metric(Metric metric) {
        this.metricId = metric.metricId;
        this.metricGoal = metric.metricGoal;
        this.metricAge = metric.metricAge;
        this.metricSex = metric.metricSex;
        this.metricWeight = metric.metricWeight;
        this.metricGrowth = metric.metricGrowth;
        this.metricHours = metric.metricHours;
        this.metricWork = metric.metricWork;
        this.metricBody = metric.metricBody;
        this.metricMeals = metric.metricMeals;
        this.wantRedMeat = metric.wantRedMeat;
        this.wantPoultry = metric.wantPoultry;
        this.wantDairyProducts = metric.wantDairyProducts;
        this.wantEggProducts = metric.wantEggProducts;
        this.wantFish = metric.wantFish;
        this.wantSeaFood = metric.wantSeaFood;
        this.wantLegume = metric.wantLegume;
        this.wantNuts = metric.wantNuts;
        this.user = new User(metric.user);
    }

    public long getMetricId() {
        return metricId;
    }

    public void setMetricId(long metricId) {
        this.metricId = metricId;
    }

    public int getMetricAge() {
        return metricAge;
    }

    public void setMetricAge(int metricAge) {
        this.metricAge = metricAge;
    }

    public int getMetricSex() {
        return metricSex;
    }

    public void setMetricSex(int metricSex) {
        this.metricSex = metricSex;
    }

    public int getMetricWeight() {
        return metricWeight;
    }

    public void setMetricWeight(int metricWeight) {
        this.metricWeight = metricWeight;
    }

    public int getMetricGrowth() {
        return metricGrowth;
    }

    public void setMetricGrowth(int metricGrowth) {
        this.metricGrowth = metricGrowth;
    }

    public int getMetricHours() {
        return metricHours;
    }

    public void setMetricHours(int metricHours) {
        this.metricHours = metricHours;
    }

    public int getMetricWork() {
        return metricWork;
    }

    public void setMetricWork(int metricWork) {
        this.metricWork = metricWork;
    }

    public int getMetricBody() {
        return metricBody;
    }

    public void setMetricBody(int metricBody) {
        this.metricBody = metricBody;
    }

    public int getMetricMeals() {
        return metricMeals;
    }

    public void setMetricMeals(int metricMeals) {
        this.metricMeals = metricMeals;
    }

    public int getWantRedMeat() {
        return wantRedMeat;
    }

    public void setWantRedMeat(int wantRedMeat) {
        this.wantRedMeat = wantRedMeat;
    }

    public int getWantPoultry() {
        return wantPoultry;
    }

    public void setWantPoultry(int wantPoultry) {
        this.wantPoultry = wantPoultry;
    }

    public int getWantDairyProducts() {
        return wantDairyProducts;
    }

    public void setWantDairyProducts(int wantDairyProducts) {
        this.wantDairyProducts = wantDairyProducts;
    }

    public int getWantEggProducts() {
        return wantEggProducts;
    }

    public void setWantEggProducts(int wantEggProducts) {
        this.wantEggProducts = wantEggProducts;
    }

    public int getWantFish() {
        return wantFish;
    }

    public void setWantFish(int wantFish) {
        this.wantFish = wantFish;
    }

    public int getWantSeaFood() {
        return wantSeaFood;
    }

    public void setWantSeaFood(int wantSeaFood) {
        this.wantSeaFood = wantSeaFood;
    }

    public int getWantLegume() {
        return wantLegume;
    }

    public void setWantLegume(int wantLegume) {
        this.wantLegume = wantLegume;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getMetricGoal() {
        return metricGoal;
    }

    public void setMetricGoal(int metricGoal) {
        this.metricGoal = metricGoal;
    }

    public int getWantNuts() {
        return wantNuts;
    }

    public void setWantNuts(int wantNuts) {
        this.wantNuts = wantNuts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Metric)) return false;
        Metric metric = (Metric) o;
        return getMetricId() == metric.getMetricId() &&
                getMetricGoal() == metric.getMetricGoal() &&
                getMetricAge() == metric.getMetricAge() &&
                getMetricSex() == metric.getMetricSex() &&
                getMetricWeight() == metric.getMetricWeight() &&
                getMetricGrowth() == metric.getMetricGrowth() &&
                getMetricHours() == metric.getMetricHours() &&
                getMetricWork() == metric.getMetricWork() &&
                getMetricBody() == metric.getMetricBody() &&
                getMetricMeals() == metric.getMetricMeals() &&
                getWantRedMeat() == metric.getWantRedMeat() &&
                getWantPoultry() == metric.getWantPoultry() &&
                getWantDairyProducts() == metric.getWantDairyProducts() &&
                getWantEggProducts() == metric.getWantEggProducts() &&
                getWantFish() == metric.getWantFish() &&
                getWantSeaFood() == metric.getWantSeaFood() &&
                getWantLegume() == metric.getWantLegume() &&
                getUser().equals(metric.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMetricId(), getMetricGoal(), getMetricAge(), getMetricSex(), getMetricWeight(), getMetricGrowth(), getMetricHours(), getMetricWork(), getMetricBody(), getMetricMeals(), getWantRedMeat(), getWantPoultry(), getWantDairyProducts(), getWantEggProducts(), getWantFish(), getWantSeaFood(), getWantLegume(), getWantNuts(), getUser());
    }

    @Override
    public String toString() {
        return "Metric{" +
                "metricId=" + metricId +
                ", metricGoal=" + metricGoal +
                ", metricAge=" + metricAge +
                ", metricSex=" + metricSex +
                ", metricWeight=" + metricWeight +
                ", metricGrowth=" + metricGrowth +
                ", metricHours=" + metricHours +
                ", metricWork=" + metricWork +
                ", metricBody=" + metricBody +
                ", metricMeals=" + metricMeals +
                ", wantRedMeat=" + wantRedMeat +
                ", wantPoultry=" + wantPoultry +
                ", wantDairyProducts=" + wantDairyProducts +
                ", wantEggProducts=" + wantEggProducts +
                ", wantFish=" + wantFish +
                ", wantSeaFood=" + wantSeaFood +
                ", wantLegume=" + wantLegume +
                ", wantNuts=" + wantNuts +
                ", user=" + user +
                '}';
    }
}
