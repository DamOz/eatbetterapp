package pl.damianozga.eatbetter.model;

import java.util.Objects;

public class Calories {
    private long caloriesId;
    private double caloriesTotal;
    private double caloriesProtein;
    private double caloriesMealProtein;
    private double caloriesCarbo;
    private double caloriesMealCarbo;
    private double caloriesFat;
    private double caloriesMealFat;
    private double caloriesFiber;
    private double caloriesWater;
    private double caloriesVegetables;
    private Metric metric;

    public Calories() {
    }

    public Calories(Calories calories) {
        this.caloriesId = calories.caloriesId;
        this.caloriesTotal = calories.caloriesTotal;
        this.caloriesProtein = calories.caloriesProtein;
        this.caloriesMealProtein = calories.caloriesMealProtein;
        this.caloriesCarbo = calories.caloriesCarbo;
        this.caloriesMealCarbo = calories.caloriesMealCarbo;
        this.caloriesFat = calories.caloriesFat;
        this.caloriesMealFat = calories.caloriesMealFat;
        this.caloriesFiber = calories.caloriesFiber;
        this.caloriesWater = calories.caloriesWater;
        this.caloriesVegetables = calories.caloriesVegetables;
        this.metric = calories.metric;
    }

    public long getCaloriesId() {
        return caloriesId;
    }

    public void setCaloriesId(long caloriesId) {
        this.caloriesId = caloriesId;
    }

    public double getCaloriesTotal() {
        return caloriesTotal;
    }

    public void setCaloriesTotal(double caloriesTotal) {
        this.caloriesTotal = caloriesTotal;
    }

    public double getCaloriesProtein() {
        return caloriesProtein;
    }

    public void setCaloriesProtein(double caloriesProtein) {
        this.caloriesProtein = caloriesProtein;
    }

    public double getCaloriesCarbo() {
        return caloriesCarbo;
    }

    public void setCaloriesCarbo(double caloriesCarbo) {
        this.caloriesCarbo = caloriesCarbo;
    }

    public double getCaloriesFat() {
        return caloriesFat;
    }

    public void setCaloriesFat(double caloriesFat) {
        this.caloriesFat = caloriesFat;
    }

    public double getCaloriesMealProtein() {
        return caloriesMealProtein;
    }

    public void setCaloriesMealProtein(double caloriesMealProtein) {
        this.caloriesMealProtein = caloriesMealProtein;
    }

    public double getCaloriesMealCarbo() {
        return caloriesMealCarbo;
    }

    public void setCaloriesMealCarbo(double caloriesMealCarbo) {
        this.caloriesMealCarbo = caloriesMealCarbo;
    }

    public double getCaloriesMealFat() {
        return caloriesMealFat;
    }

    public void setCaloriesMealFat(double caloriesMealFat) {
        this.caloriesMealFat = caloriesMealFat;
    }

    public double getCaloriesFiber() {
        return caloriesFiber;
    }

    public void setCaloriesFiber(double caloriesFiber) {
        this.caloriesFiber = caloriesFiber;
    }

    public double getCaloriesWater() {
        return caloriesWater;
    }

    public void setCaloriesWater(double caloriesWater) {
        this.caloriesWater = caloriesWater;
    }

    public double getCaloriesVegetables() {
        return caloriesVegetables;
    }

    public void setCaloriesVegetables(double caloriesVegetables) {
        this.caloriesVegetables = caloriesVegetables;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Calories)) return false;
        Calories calories = (Calories) o;
        return getCaloriesId() == calories.getCaloriesId() &&
                Double.compare(calories.getCaloriesTotal(), getCaloriesTotal()) == 0 &&
                Double.compare(calories.getCaloriesProtein(), getCaloriesProtein()) == 0 &&
                Double.compare(calories.getCaloriesMealProtein(), getCaloriesMealProtein()) == 0 &&
                Double.compare(calories.getCaloriesCarbo(), getCaloriesCarbo()) == 0 &&
                Double.compare(calories.getCaloriesMealCarbo(), getCaloriesMealCarbo()) == 0 &&
                Double.compare(calories.getCaloriesFat(), getCaloriesFat()) == 0 &&
                Double.compare(calories.getCaloriesMealFat(), getCaloriesMealFat()) == 0 &&
                Double.compare(calories.getCaloriesFiber(), getCaloriesFiber()) == 0 &&
                Double.compare(calories.getCaloriesWater(), getCaloriesWater()) == 0 &&
                Double.compare(calories.getCaloriesVegetables(), getCaloriesVegetables()) == 0 &&
                getMetric().equals(calories.getMetric());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCaloriesId(), getCaloriesTotal(), getCaloriesProtein(), getCaloriesMealProtein(), getCaloriesCarbo(), getCaloriesMealCarbo(), getCaloriesFat(), getCaloriesMealFat(), getCaloriesFiber(), getCaloriesWater(), getCaloriesVegetables(), getMetric());
    }

    @Override
    public String toString() {
        return "Calories{" +
                "caloriesId=" + caloriesId +
                ", caloriesTotal=" + caloriesTotal +
                ", caloriesProtein=" + caloriesProtein +
                ", caloriesMealProtein=" + caloriesMealProtein +
                ", caloriesCarbo=" + caloriesCarbo +
                ", caloriesMealCarbo=" + caloriesMealCarbo +
                ", caloriesFat=" + caloriesFat +
                ", caloriesMealFat=" + caloriesMealFat +
                ", caloriesFiber=" + caloriesFiber +
                ", caloriesWater=" + caloriesWater +
                ", caloriesVegetables=" + caloriesVegetables +
                ", metric=" + metric +
                '}';
    }
}
