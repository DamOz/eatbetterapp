package pl.damianozga.eatbetter.model;

import java.util.Objects;

public class Product {
    private long productId;
    private String productName;
    private int productProtein;
    private int productCarbo;
    private int productFat;
    private int productFiber;
    private int productCategory;

    public Product() {
    }

    public Product(Product product) {
        this.productId = product.productId;
        this.productName = product.productName;
        this.productProtein = product.productProtein;
        this.productCarbo = product.productCarbo;
        this.productFat = product.productFat;
        this.productFiber = product.productFiber;
        this.productCategory = product.productCategory;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductProtein() {
        return productProtein;
    }

    public void setProductProtein(int productProtein) {
        this.productProtein = productProtein;
    }

    public int getProductCarbo() {
        return productCarbo;
    }

    public void setProductCarbo(int productCarbo) {
        this.productCarbo = productCarbo;
    }

    public int getProductFat() {
        return productFat;
    }

    public void setProductFat(int productFat) {
        this.productFat = productFat;
    }

    public int getProductFiber() {
        return productFiber;
    }

    public void setProductFiber(int productFiber) {
        this.productFiber = productFiber;
    }

    public int getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(int productCategory) {
        this.productCategory = productCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getProductId() == product.getProductId() &&
                getProductProtein() == product.getProductProtein() &&
                getProductCarbo() == product.getProductCarbo() &&
                getProductFat() == product.getProductFat() &&
                getProductFiber() == product.getProductFiber() &&
                getProductCategory() == product.getProductCategory() &&
                getProductName().equals(product.getProductName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId(), getProductName(), getProductProtein(), getProductCarbo(), getProductFat(), getProductFiber(), getProductCategory());
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productProtein=" + productProtein +
                ", productCarbo=" + productCarbo +
                ", productFat=" + productFat +
                ", productFiber=" + productFiber +
                ", productCategory=" + productCategory +
                '}';
    }
}
