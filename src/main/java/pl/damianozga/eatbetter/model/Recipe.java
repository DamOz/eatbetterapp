package pl.damianozga.eatbetter.model;


import java.sql.Timestamp;
import java.util.Objects;

public class Recipe {

    private Long recipeId;
    private String recipeName;
    private String recipeDescription;
    private String recipeIg1;
    private String recipeIg2;
    private String recipeIg3;
    private String recipeIg4;
    private String recipeIg5;
    private String recipeIg6;
    private String recipeIg7;
    private String recipeIg8;
    private String recipeIg9;
    private String recipeIg10;
    private String recipeImg;
    private String recipeContent;
    private Timestamp recipeTimestamp;

    public Recipe() {
    }

    public Recipe(Recipe recipe) {
        this.recipeId = recipe.recipeId;
        this.recipeName = recipe.recipeName;
        this.recipeDescription = recipe.recipeDescription;
        this.recipeIg1 = recipe.recipeIg1;
        this.recipeIg2 = recipe.recipeIg2;
        this.recipeIg3 = recipe.recipeIg3;
        this.recipeIg4 = recipe.recipeIg4;
        this.recipeIg5 = recipe.recipeIg5;
        this.recipeIg6 = recipe.recipeIg6;
        this.recipeIg7 = recipe.recipeIg7;
        this.recipeIg8 = recipe.recipeIg8;
        this.recipeIg9 = recipe.recipeIg9;
        this.recipeIg10 = recipe.recipeIg10;
        this.recipeImg = recipe.recipeImg;
        this.recipeContent = recipe.recipeContent;
        this.recipeTimestamp = new Timestamp(recipe.recipeTimestamp.getTime());
    }


    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipeDescription() {
        return recipeDescription;
    }

    public void setRecipeDescription(String recipeDescription) {
        this.recipeDescription = recipeDescription;
    }

    public String getRecipeIg1() {
        return recipeIg1;
    }

    public void setRecipeIg1(String recipeIg1) {
        this.recipeIg1 = recipeIg1;
    }

    public String getRecipeIg2() {
        return recipeIg2;
    }

    public void setRecipeIg2(String recipeIg2) {
        this.recipeIg2 = recipeIg2;
    }

    public String getRecipeIg3() {
        return recipeIg3;
    }

    public void setRecipeIg3(String recipeIg3) {
        this.recipeIg3 = recipeIg3;
    }

    public String getRecipeIg4() {
        return recipeIg4;
    }

    public void setRecipeIg4(String recipeIg4) {
        this.recipeIg4 = recipeIg4;
    }

    public String getRecipeIg5() {
        return recipeIg5;
    }

    public void setRecipeIg5(String recipeIg5) {
        this.recipeIg5 = recipeIg5;
    }

    public String getRecipeIg6() {
        return recipeIg6;
    }

    public void setRecipeIg6(String recipeIg6) {
        this.recipeIg6 = recipeIg6;
    }

    public String getRecipeIg7() {
        return recipeIg7;
    }

    public void setRecipeIg7(String recipeIg7) {
        this.recipeIg7 = recipeIg7;
    }

    public String getRecipeIg8() {
        return recipeIg8;
    }

    public void setRecipeIg8(String recipeIg8) {
        this.recipeIg8 = recipeIg8;
    }

    public String getRecipeIg9() {
        return recipeIg9;
    }

    public void setRecipeIg9(String recipeIg9) {
        this.recipeIg9 = recipeIg9;
    }

    public String getRecipeIg10() {
        return recipeIg10;
    }

    public void setRecipeIg10(String recipeIg10) {
        this.recipeIg10 = recipeIg10;
    }

    public String getRecipeImg() {
        return recipeImg;
    }

    public void setRecipeImg(String recipeImg) {
        this.recipeImg = recipeImg;
    }

    public String getRecipeContent() {
        return recipeContent;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }

    public Timestamp getRecipeTimestamp() {
        return recipeTimestamp;
    }

    public void setRecipeTimestamp(Timestamp recipeTimestamp) {
        this.recipeTimestamp = recipeTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Recipe)) return false;
        Recipe recipe = (Recipe) o;
        return Objects.equals(getRecipeId(), recipe.getRecipeId()) &&
                Objects.equals(getRecipeName(), recipe.getRecipeName()) &&
                Objects.equals(getRecipeDescription(), recipe.getRecipeDescription()) &&
                Objects.equals(getRecipeIg1(), recipe.getRecipeIg1()) &&
                Objects.equals(getRecipeIg2(), recipe.getRecipeIg2()) &&
                Objects.equals(getRecipeIg3(), recipe.getRecipeIg3()) &&
                Objects.equals(getRecipeIg4(), recipe.getRecipeIg4()) &&
                Objects.equals(getRecipeIg5(), recipe.getRecipeIg5()) &&
                Objects.equals(getRecipeIg6(), recipe.getRecipeIg6()) &&
                Objects.equals(getRecipeIg7(), recipe.getRecipeIg7()) &&
                Objects.equals(getRecipeIg8(), recipe.getRecipeIg8()) &&
                Objects.equals(getRecipeIg9(), recipe.getRecipeIg9()) &&
                Objects.equals(getRecipeIg10(), recipe.getRecipeIg10()) &&
                Objects.equals(getRecipeImg(), recipe.getRecipeImg()) &&
                Objects.equals(getRecipeContent(), recipe.getRecipeContent()) &&
                Objects.equals(getRecipeTimestamp(), recipe.getRecipeTimestamp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRecipeId(), getRecipeName(), getRecipeDescription(), getRecipeIg1(), getRecipeIg2(), getRecipeIg3(), getRecipeIg4(), getRecipeIg5(), getRecipeIg6(), getRecipeIg7(), getRecipeIg8(), getRecipeIg9(), getRecipeIg10(), getRecipeImg(), getRecipeContent(), getRecipeTimestamp());
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "recipeId=" + recipeId +
                ", recipeName='" + recipeName + '\'' +
                ", recipeDescription='" + recipeDescription + '\'' +
                ", recipeIg1='" + recipeIg1 + '\'' +
                ", recipeIg2='" + recipeIg2 + '\'' +
                ", recipeIg3='" + recipeIg3 + '\'' +
                ", recipeIg4='" + recipeIg4 + '\'' +
                ", recipeIg5='" + recipeIg5 + '\'' +
                ", recipeIg6='" + recipeIg6 + '\'' +
                ", recipeIg7='" + recipeIg7 + '\'' +
                ", recipeIg8='" + recipeIg8 + '\'' +
                ", recipeIg9='" + recipeIg9 + '\'' +
                ", recipeIg10='" + recipeIg10 + '\'' +
                ", recipeImg='" + recipeImg + '\'' +
                ", recipeContent='" + recipeContent + '\'' +
                ", recipeTimestamp=" + recipeTimestamp +
                '}';
    }
}