package pl.damianozga.eatbetter.model;

import java.util.Objects;

public class Plan {

    private long planId;
    private String planName;
    private double planProtein;
    private double planCarbo;
    private double planFat;
    private double planFiber;
    private double planCategory;
    private double planGroup;
    private Calories calories;

    public Plan() {
    }

    public Plan(Plan plan) {
        this.planId = plan.planId;
        this.planName = plan.planName;
        this.planProtein = plan.planProtein;
        this.planCarbo = plan.planCarbo;
        this.planFat = plan.planFat;
        this.planFiber = plan.planFiber;
        this.planCategory = plan.planCategory;
        this.planGroup = plan.planGroup;
        this.calories = new Calories(plan.calories);
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public double getPlanProtein() {
        return planProtein;
    }

    public void setPlanProtein(double planProtein) {
        this.planProtein = planProtein;
    }

    public double getPlanCarbo() {
        return planCarbo;
    }

    public void setPlanCarbo(double planCarbo) {
        this.planCarbo = planCarbo;
    }

    public double getPlanFat() {
        return planFat;
    }

    public void setPlanFat(double planFat) {
        this.planFat = planFat;
    }

    public double getPlanFiber() {
        return planFiber;
    }

    public void setPlanFiber(double planFiber) {
        this.planFiber = planFiber;
    }

    public double getPlanCategory() {
        return planCategory;
    }

    public void setPlanCategory(double planCategory) {
        this.planCategory = planCategory;
    }

    public double getPlanGroup() {
        return planGroup;
    }

    public void setPlanGroup(double planGroup) {
        this.planGroup = planGroup;
    }

    public Calories getCalories() {
        return calories;
    }

    public void setCalories(Calories calories) {
        this.calories = calories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plan)) return false;
        Plan plan = (Plan) o;
        return getPlanId() == plan.getPlanId() &&
                Double.compare(plan.getPlanProtein(), getPlanProtein()) == 0 &&
                Double.compare(plan.getPlanCarbo(), getPlanCarbo()) == 0 &&
                Double.compare(plan.getPlanFat(), getPlanFat()) == 0 &&
                Double.compare(plan.getPlanFiber(), getPlanFiber()) == 0 &&
                Double.compare(plan.getPlanCategory(), getPlanCategory()) == 0 &&
                Double.compare(plan.getPlanGroup(), getPlanGroup()) == 0 &&
                getPlanName().equals(plan.getPlanName()) &&
                getCalories().equals(plan.getCalories());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlanId(), getPlanName(), getPlanProtein(), getPlanCarbo(), getPlanFat(), getPlanFiber(), getPlanCategory(), getPlanGroup(), getCalories());
    }

    @Override
    public String toString() {
        return "Plan{" +
                "planId=" + planId +
                ", planName='" + planName + '\'' +
                ", planProtein=" + planProtein +
                ", planCarbo=" + planCarbo +
                ", planFat=" + planFat +
                ", planFiber=" + planFiber +
                ", planCategory=" + planCategory +
                ", planGroup=" + planGroup +
                ", calories=" + calories +
                '}';
    }
}
