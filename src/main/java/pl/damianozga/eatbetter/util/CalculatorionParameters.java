package pl.damianozga.eatbetter.util;

public class CalculatorionParameters {

    private double percentOfProtein = 0.2;
    private double percentOfCarbo = 0.4;
    private double percentOfFat = 0.4;
    private double amountOfFiber = 60;
    private double proteinKcalToGrams = 4;
    private double carboKcalToGrams = 4;
    private double fatKcalToGrams = 9;

    public double getPercentOfProtein() {
        return percentOfProtein;
    }

    public void setPercentOfProtein(double percentOfProtein) {
        this.percentOfProtein = percentOfProtein;
    }

    public double getPercentOfCarbo() {
        return percentOfCarbo;
    }

    public void setPercentOfCarbo(double percentOfCarbo) {
        this.percentOfCarbo = percentOfCarbo;
    }

    public double getPercentOfFat() {
        return percentOfFat;
    }

    public void setPercentOfFat(double percentOfFat) {
        this.percentOfFat = percentOfFat;
    }

    public double getAmountOfFiber() {
        return amountOfFiber;
    }

    public void setAmountOfFiber(double amountOfFiber) {
        this.amountOfFiber = amountOfFiber;
    }

    public double getProteinKcalToGrams() {
        return proteinKcalToGrams;
    }

    public void setProteinKcalToGrams(double proteinKcalToGrams) {
        this.proteinKcalToGrams = proteinKcalToGrams;
    }

    public double getCarboKcalToGrams() {
        return carboKcalToGrams;
    }

    public void setCarboKcalToGrams(double carboKcalToGrams) {
        this.carboKcalToGrams = carboKcalToGrams;
    }

    public double getFatKcalToGrams() {
        return fatKcalToGrams;
    }

    public void setFatKcalToGrams(double fatKcalToGrams) {
        this.fatKcalToGrams = fatKcalToGrams;
    }
}
