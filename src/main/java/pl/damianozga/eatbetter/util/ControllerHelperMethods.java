package pl.damianozga.eatbetter.util;

import pl.damianozga.eatbetter.model.*;
import pl.damianozga.eatbetter.patterns.TotalCaloriesPattern;
import pl.damianozga.eatbetter.patterns.UserProductsPattern;
import pl.damianozga.eatbetter.service.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ControllerHelperMethods {

    public void getUserObject(HttpServletRequest request) throws SQLException {
        User user = (User) request.getSession().getAttribute("user");
        request.setAttribute("user", user);
    }

    public void getMetricObject(HttpServletRequest request) throws SQLException {
        MetricService metricService = new MetricService();
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        Metric metricSummary = metricService.getMetricById(user_id);
        MetricDataConverter metricDataConverter = new MetricDataConverter();
        request.setAttribute("summary", metricSummary);
        request.setAttribute("converter", metricDataConverter);

    }

    public void getCaloriesObject(HttpServletRequest request) throws SQLException {
        User tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        MetricService metricService = new MetricService();
        Metric tempMetric = metricService.getMetricById(user_id);
        long metric_id = tempMetric.getMetricId();
        CaloriesService caloriesService = new CaloriesService();
        Calories calories = caloriesService.getCaloriesById(metric_id);
        request.setAttribute("calories", calories);
    }

    public void getPlanObject(HttpServletRequest request) throws SQLException {
        User tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        MetricService metricService = new MetricService();
        Metric tempMetric = metricService.getMetricById(user_id);
        long metric_id = tempMetric.getMetricId();
        CaloriesService caloriesService = new CaloriesService();
        Calories tempCalories = caloriesService.getCaloriesById(metric_id);
        long calories_id = tempCalories.getCaloriesId();
        PlanService planService = new PlanService();
        List<Plan> plan = planService.getPlan(calories_id);
        request.setAttribute("plans", plan);
    }

    public void deleteMetric(HttpServletRequest request) throws SQLException {
        MetricService metricService = new MetricService();
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        Metric tempMetric = metricService.getMetricById(user_id);
        int metric_id = (int) tempMetric.getMetricId();
        metricService.deleteMetric(metric_id);
    }

    public void deleteCalories(HttpServletRequest request) throws SQLException {
        MetricService metricService = new MetricService();
        CaloriesService caloriesService = new CaloriesService();
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        Metric tempMetric = metricService.getMetricById(user_id);
        int metric_id = (int) tempMetric.getMetricId();
        Calories tempCalories = caloriesService.getCaloriesByIdForDelete(metric_id);
        int calories_id = (int) tempCalories.getCaloriesId();
        caloriesService.deleteCalories(calories_id);
    }

    public void deletePlan(HttpServletRequest request) throws SQLException {
        MetricService metricService = new MetricService();
        CaloriesService caloriesService = new CaloriesService();
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        Metric tempMetric = metricService.getMetricById(user_id);
        int metric_id = (int) tempMetric.getMetricId();
        Calories tempCalories = caloriesService.getCaloriesByIdForDelete(metric_id);
        int calories_id = (int) tempCalories.getCaloriesId();
        PlanService planService = new PlanService();
        planService.deletePlan(calories_id);
    }

    public void caloriesCalculation(HttpServletRequest request) throws SQLException {
        MetricService metricService = new MetricService();
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        Metric tempMetric = metricService.getMetricById(user_id);
        TotalCaloriesPattern totalCaloriesPattern = new TotalCaloriesPattern();
        double sex = totalCaloriesPattern.sexFormula(tempMetric);
        double weight = totalCaloriesPattern.weightFormula(tempMetric);
        double growth = totalCaloriesPattern.growthFormula(tempMetric);
        double age = totalCaloriesPattern.ageFormula(tempMetric);
        double work = totalCaloriesPattern.workFormula(tempMetric);
        double hours = totalCaloriesPattern.hoursFormula(tempMetric);
        double goal = totalCaloriesPattern.goalFormula(tempMetric);
        double total = totalCaloriesPattern.totalFormula(sex, weight, growth, age, work, hours, goal);
        CalculatorionParameters cP = new CalculatorionParameters();
        double caloriesTotal = total;
        double caloriesProtein = total * cP.getPercentOfProtein();
        double caloriesMealProtein = caloriesProtein / tempMetric.getMetricMeals() / cP.getProteinKcalToGrams();
        double caloriesCarbo = total * cP.getPercentOfCarbo() - cP.getAmountOfFiber();
        double caloriesMealCarbo = caloriesCarbo / tempMetric.getMetricMeals() / cP.getCarboKcalToGrams();
        double caloriesFat = total * cP.getPercentOfFat();
        double caloriesMealFat = caloriesFat / tempMetric.getMetricMeals() / cP.getFatKcalToGrams();
        double caloriesFiber = cP.getAmountOfFiber();
        double caloriesWater = totalCaloriesPattern.waterFormula(tempMetric);
        int caloriesVegetables = (int) totalCaloriesPattern.vegetablesFormula(tempMetric);
        CaloriesService caloriesService = new CaloriesService();
        caloriesService.addCalories(caloriesTotal, caloriesProtein, caloriesMealProtein,
                caloriesCarbo, caloriesMealCarbo, caloriesFat, caloriesMealFat, caloriesFiber,
                caloriesWater, caloriesVegetables, tempMetric);
    }

    public boolean caloriesExists(HttpServletRequest request) throws SQLException {
        MetricService metricService = new MetricService();
        CaloriesService caloriesService = new CaloriesService();
        User tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        Metric tempMetric = metricService.getMetricById(user_id);
        long metric_id = tempMetric.getMetricId();
        Calories tempCalories = caloriesService.getCaloriesByIdForExist(metric_id);
        boolean exists;
        if (tempCalories == null) {
            exists = false;
        } else
            exists = true;
        return exists;
    }

    public boolean metricExists(HttpServletRequest request) throws SQLException {
        MetricService metricService = new MetricService();
        User tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        Metric tempMetric = metricService.getMetricByIdForExist(user_id);
        boolean exists;
        if (tempMetric != null) {
            exists = true;
        } else
            exists = false;
        return exists;
    }

    public void planCalculation(HttpServletRequest request) throws SQLException {
        ProductService productService = new ProductService();
        List<Product> tempList = productService.getAllProducts();
        User tempUser = (User) request.getSession().getAttribute("user");
        int user_id = (int) tempUser.getId();
        MetricService metricService = new MetricService();
        Metric tempMetric = metricService.getMetricById(user_id);
        long metric_id = tempMetric.getMetricId();
        CaloriesService caloriesService = new CaloriesService();
        Calories tempCalories = caloriesService.getCaloriesById(metric_id);
        UserProductsPattern userProductsPattern = new UserProductsPattern();
        ArrayList<Plan> tempArrayPlanList;
        tempArrayPlanList = userProductsPattern.userProductsCalculation(tempList, tempCalories, tempUser);
        PlanService planService = new PlanService();
        for (int i = 0; i < tempArrayPlanList.size(); i++) {
            Plan tempPlan = tempArrayPlanList.get(i);
            planService.addPlan(tempPlan.getPlanName(), tempPlan.getPlanProtein(), tempPlan.getPlanCarbo(),
                    tempPlan.getPlanFat(), tempPlan.getPlanFiber(), tempPlan.getPlanCategory(), tempPlan.getPlanGroup(),
                    tempPlan.getCalories());
        }

    }

    public void getAllRecipesObjects(HttpServletRequest request) throws SQLException {
        RecipeService recipeService = new RecipeService();
        ArrayList<Recipe> tempRecipesList = (ArrayList<Recipe>) recipeService.getRecipes();
        ArrayList<Recipe> resultRecipesList = new ArrayList<>();
        for (int i = 0; i < tempRecipesList.size(); i++) {
            Recipe tempRecipes = tempRecipesList.get(i);
            resultRecipesList.add(tempRecipes);
        }
        request.setAttribute("recipes", resultRecipesList);
    }

}
