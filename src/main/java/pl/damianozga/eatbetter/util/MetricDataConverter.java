package pl.damianozga.eatbetter.util;

public class MetricDataConverter {

    public String showGoal(int goal) {
        String result = null;
        switch (goal) {
            case 1:
                result = "Redukcja masy ciała";
                break;
            case 2:
                result = "Zwiększenie masy ciał";
                break;
            case 3:
                result = "Utrzymanie stanu obecnego";
        }
        return result;
    }

    public String showSex(int sex) {
        String result = null;
        switch (sex) {
            case 1:
                result = "Kobieta";
                break;
            case 0:
                result = "Mężczyzna";
                break;
        }
        return result;
    }

    public String showHours(int hours) {
        String result = null;
        switch (hours) {
            case 0:
                result = "Nie trenuję";
                break;
            case 1:
                result = "1h - 3h (dwa treningi)";
                break;
            case 2:
                result = "4h - 6h (trzy treningi)";
                break;
            case 3:
                result = "7h - 9h (cztery treningi)";
                break;
            case 4:
                result = "10h - 12h (pięć treningów)";
                break;
            case 5:
                result = "13h - 15h (sześć treningów)";
        }
        return result;
    }

    public String showWork(int work) {
        String result = null;
        switch (work) {
            case 1:
                result = "Nie pracuje";
                break;
            case 2:
                result = "Praca siedząca";
                break;
            case 3:
                result = "Praca stojąca, nie fizyczna";
                break;
            case 4:
                result = "Lekka praca fizyczna";
                break;
            case 5:
                result = "Ciężka praca fizyczna";
        }
        return result;
    }

    public String showBody(int body) {
        String result = null;
        switch (body) {
            case 1:
                result = "Ektomorfik";
                break;
            case 2:
                result = "Mezomorfik";
                break;
            case 3:
                result = "Endomorfik";
        }
        return result;
    }

    public String showForeclosure(int foreclosure) {
        String result;
        if (foreclosure != 1) {
            result = "Nie chcę";
        } else
            result = "Chcę";
        return result;
    }
}
