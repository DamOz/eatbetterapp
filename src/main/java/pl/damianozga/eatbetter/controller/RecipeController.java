package pl.damianozga.eatbetter.controller;

import pl.damianozga.eatbetter.dao.RecipeDAO;
import pl.damianozga.eatbetter.model.Recipe;
import pl.damianozga.eatbetter.service.RecipeService;
import pl.damianozga.eatbetter.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet("/recipe")
public class RecipeController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/new.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String recipeName = req.getParameter("inputRecipeName");
        String recipeDescription = req.getParameter("inputRecipeDescription");
        String recipeIg1 = req.getParameter("inputRecipeIg1");
        String recipeIg2 = req.getParameter("inputRecipeIg2");
        String recipeIg3 = req.getParameter("inputRecipeIg3");
        String recipeIg4 = req.getParameter("inputRecipeIg4");
        String recipeIg5 = req.getParameter("inputRecipeIg5");
        String recipeIg6 = req.getParameter("inputRecipeIg6");
        String recipeIg7 = req.getParameter("inputRecipeIg7");
        String recipeIg8 = req.getParameter("inputRecipeIg8");
        String recipeIg9 = req.getParameter("inputRecipeIg9");
        String recipeIg10 = req.getParameter("inputRecipeIg10");
        String recipeImg = req.getParameter("inputRecipeImg");
        String recipeContent = req.getParameter("inputRecipeContent");
        RecipeService recipeService = new RecipeService();
        recipeService.addRecipe(recipeName, recipeDescription, recipeIg1, recipeIg2, recipeIg3, recipeIg4,
                recipeIg5, recipeIg6, recipeIg7, recipeIg8, recipeIg9, recipeIg10, recipeImg, recipeContent);
        resp.sendRedirect("/");
    }
}