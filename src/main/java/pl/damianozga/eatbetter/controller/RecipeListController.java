package pl.damianozga.eatbetter.controller;

import pl.damianozga.eatbetter.util.ControllerHelperMethods;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/recipelist")
public class RecipeListController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ControllerHelperMethods cHM = new ControllerHelperMethods();
        try {
            cHM.getAllRecipesObjects(req);
            req.getRequestDispatcher("WEB-INF/recipes-list.jsp").forward(req, resp);
        } catch (SQLException e) {
            req.getRequestDispatcher("/error").forward(req, resp);
        }

    }

}
