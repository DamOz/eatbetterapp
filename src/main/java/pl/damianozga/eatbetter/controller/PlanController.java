package pl.damianozga.eatbetter.controller;


import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import pl.damianozga.eatbetter.model.Calories;
import pl.damianozga.eatbetter.model.Metric;
import pl.damianozga.eatbetter.model.Plan;
import pl.damianozga.eatbetter.model.User;
import pl.damianozga.eatbetter.pdf.PdfPlanCreator;
import pl.damianozga.eatbetter.service.CaloriesService;
import pl.damianozga.eatbetter.service.MetricService;
import pl.damianozga.eatbetter.service.PlanService;
import pl.damianozga.eatbetter.util.ControllerHelperMethods;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Path;
import java.sql.SQLException;


@WebServlet("/plan")
public class PlanController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            ControllerHelperMethods cHM = new ControllerHelperMethods();
            if (cHM.metricExists(req)) {
                cHM.getUserObject(req);
                cHM.getMetricObject(req);
                cHM.getCaloriesObject(req);
                cHM.getPlanObject(req);
                req.getRequestDispatcher("/WEB-INF/plan.jsp").forward(req, resp);

            } else {
                req.getRequestDispatcher("/metric").forward(req, resp);
            }
        } catch (SQLException e) {
            req.getRequestDispatcher("/error").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int option = Integer.parseInt(req.getParameter("delete"));
        if (option != 1) {

            resp.sendRedirect("/plan");
        } else {
            try {
                PdfPlanCreator ppc = new PdfPlanCreator();
                byte[] plan = ppc.createPlan(req);
                ppc.getPlan(req, resp, plan);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            req.getRequestDispatcher("/WEB-INF/metric.jsp").forward(req, resp);
        }
    }


}
