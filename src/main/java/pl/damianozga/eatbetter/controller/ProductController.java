package pl.damianozga.eatbetter.controller;

import pl.damianozga.eatbetter.dao.ProductDAOImpl;
import pl.damianozga.eatbetter.service.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/new-product")
public class ProductController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/new-product.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String productName = req.getParameter("inputProductName");
        int productProtein = Integer.parseInt(req.getParameter("inputProductProtein"));
        int productCarbo = Integer.parseInt(req.getParameter("inputProductCarbo"));
        int productFat = Integer.parseInt(req.getParameter("inputProductFat"));
        int productFiber = Integer.parseInt(req.getParameter("inputProductFiber"));
        int productCategory = Integer.parseInt(req.getParameter("inputProductCategory"));
        ProductService productService = new ProductService();
        productService.addProduct(productName, productProtein, productCarbo, productFat,
                productFiber, productCategory);
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
