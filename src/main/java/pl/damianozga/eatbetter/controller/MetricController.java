package pl.damianozga.eatbetter.controller;

import pl.damianozga.eatbetter.model.Metric;
import pl.damianozga.eatbetter.model.User;
import pl.damianozga.eatbetter.service.MetricService;
import pl.damianozga.eatbetter.util.ControllerHelperMethods;
import pl.damianozga.eatbetter.util.IntegerParser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/metric")
public class MetricController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            ControllerHelperMethods cHM = new ControllerHelperMethods();
            if (cHM.metricExists(req)) {
                req.getRequestDispatcher("/summary").forward(req, resp);
            } else {
                req.getRequestDispatcher("/WEB-INF/metric.jsp").forward(req, resp);
            }
        } catch (SQLException e) {
            req.getRequestDispatcher("/error").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        IntegerParser integerParser = new IntegerParser();
        int metricGoal = Integer.parseInt(req.getParameter("InputMetricGoal"));
        int metricAge = Integer.parseInt(req.getParameter("InputMetricAge"));
        int metricSex = Integer.parseInt(req.getParameter("InputMetricSex"));
        int metricWeight = Integer.parseInt(req.getParameter("InputMetricWeight"));
        int metricGrowth = Integer.parseInt(req.getParameter("InputMetricGrowth"));
        int metricHours = Integer.parseInt(req.getParameter("InputMetricHours"));
        int metricWork = Integer.parseInt(req.getParameter("InputMetricWork"));
        int metricBody = Integer.parseInt(req.getParameter("InputMetricBody"));
        int metricMeals = Integer.parseInt(req.getParameter("InputMetricMeals"));
        String mRedMeat = req.getParameter("InputMetricRedMeat");
        int metricRedMeat = integerParser.integerNullParser(mRedMeat);
        String mPoultry = req.getParameter("InputMetricPoultry");
        int metricPoultry = integerParser.integerNullParser(mPoultry);
        String mDairyProducts = req.getParameter("InputMetricDairyProducts");
        int metricDairyProducts = integerParser.integerNullParser(mDairyProducts);
        String mEgg = req.getParameter("InputEggProducts");
        int metricEggProducts = integerParser.integerNullParser(mEgg);
        String mFish = req.getParameter("InputMetricFish");
        int metricFish = integerParser.integerNullParser(mFish);
        String mSeaFood = req.getParameter("InputMetricSeaFood");
        int metricSeaFood = integerParser.integerNullParser(mSeaFood);
        String mLegume = req.getParameter("InputMetricLegume");
        int metricLegume = integerParser.integerNullParser(mLegume);
        String mNuts = req.getParameter("InputMetricNuts");
        int metricNuts = integerParser.integerNullParser(mNuts);
        User authenticatedUser = (User) req.getSession().getAttribute("user");
        if (req.getUserPrincipal() != null) {
            MetricService metricService = new MetricService();
            metricService.addMetric(metricGoal, metricAge, metricSex, metricWeight, metricGrowth, metricHours, metricWork,
                    metricBody, metricMeals, metricRedMeat, metricPoultry, metricDairyProducts, metricEggProducts,
                    metricFish, metricSeaFood, metricLegume, metricNuts, authenticatedUser);
            resp.sendRedirect(req.getContextPath() + "/summary");
        } else {
            resp.sendError(403);
        }
    }

}
