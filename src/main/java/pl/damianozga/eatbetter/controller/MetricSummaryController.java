package pl.damianozga.eatbetter.controller;

import pl.damianozga.eatbetter.model.*;
import pl.damianozga.eatbetter.patterns.TotalCaloriesPattern;
import pl.damianozga.eatbetter.patterns.UserProductsPattern;
import pl.damianozga.eatbetter.service.*;
import pl.damianozga.eatbetter.util.CalculatorionParameters;
import pl.damianozga.eatbetter.util.ControllerHelperMethods;
import pl.damianozga.eatbetter.util.IntegerParser;
import pl.damianozga.eatbetter.util.MetricDataConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/summary")
public class MetricSummaryController extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            ControllerHelperMethods cHM = new ControllerHelperMethods();
            if (cHM.metricExists(req)) {
                cHM.getMetricObject(req);
                try {
                    if (!cHM.caloriesExists(req)) {
                        cHM.caloriesCalculation(req);
                        cHM.planCalculation(req);
                        req.getRequestDispatcher("/WEB-INF/metric-summary.jsp").forward(req, resp);
                    } else
                        req.getRequestDispatcher("/WEB-INF/metric-summary.jsp").forward(req, resp);
                } catch (SQLException e) {
                    req.getRequestDispatcher("/error").forward(req, resp);
                }
            } else
                req.getRequestDispatcher("/metric").forward(req, resp);
        } catch (SQLException e) {
            req.getRequestDispatcher("/error").forward(req, resp);
        }

    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int option = Integer.parseInt(req.getParameter("delete"));
        ControllerHelperMethods cHM = new ControllerHelperMethods();
        if (option != 1) {

            resp.sendRedirect("/plan");
        } else {
            try {
                cHM.deletePlan(req);
                cHM.deleteCalories(req);
                cHM.deleteMetric(req);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            req.getRequestDispatcher("/WEB-INF/metric.jsp").forward(req, resp);
        }
    }


}
