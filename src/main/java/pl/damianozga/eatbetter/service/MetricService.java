package pl.damianozga.eatbetter.service;

import pl.damianozga.eatbetter.dao.DAOFactory;
import pl.damianozga.eatbetter.dao.MetricDAO;
import pl.damianozga.eatbetter.model.Metric;
import pl.damianozga.eatbetter.model.User;

import java.sql.SQLException;

public class MetricService {

    public void addMetric(int metricGoal, int metricAge, int metricSex, int metricWeight, int metricGrowth, int metricHours, int metricWork,
                          int metricBody, int metricMeals, int wantRedMeat, int wantPoultry, int wantDairyProducts,
                          int wantEggProducts, int wantFish, int wantSeaFood, int wantLegume, int wantNuts, User user) {
        Metric metric = createMetricObject(metricGoal, metricAge, metricSex, metricWeight, metricGrowth, metricHours, metricWork,
                metricBody, metricMeals, wantRedMeat, wantPoultry, wantDairyProducts, wantEggProducts, wantFish, wantSeaFood,
                wantLegume, wantNuts, user);

        DAOFactory factory = DAOFactory.getDAOFactory();
        MetricDAO metricDao = factory.getMetricDAO();
        metricDao.create(metric);

    }

    private Metric createMetricObject(int metricGoal, int metricAge, int metricSex, int metricWeight, int metricGrowth, int metricHours,
                                      int metricWork, int metricBody, int metricMeals, int wantRedMeat, int wantPoultry,
                                      int wantDairyProducts, int wantEggProducts, int wantFish, int wantSeaFood, int wantLegume,
                                      int wantNuts, User user) {
        Metric metric = new Metric();
        metric.setMetricGoal(metricGoal);
        metric.setMetricAge(metricAge);
        metric.setMetricSex(metricSex);
        metric.setMetricWeight(metricWeight);
        metric.setMetricGrowth(metricGrowth);
        metric.setMetricHours(metricHours);
        metric.setMetricWork(metricWork);
        metric.setMetricBody(metricBody);
        metric.setMetricMeals(metricMeals);
        metric.setWantRedMeat(wantRedMeat);
        metric.setWantPoultry(wantPoultry);
        metric.setWantDairyProducts(wantDairyProducts);
        metric.setWantEggProducts(wantEggProducts);
        metric.setWantFish(wantFish);
        metric.setWantSeaFood(wantSeaFood);
        metric.setWantLegume(wantLegume);
        metric.setWantNuts(wantNuts);
        User userCopy = new User(user);
        metric.setUser(userCopy);
        return metric;
    }

    public void deleteMetric(int metricId) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        MetricDAO metricDAO = factory.getMetricDAO();
        Metric metric = metricDAO.readForDelete(metricId);
        int tempMetricId = (int) metric.getMetricId();
        metricDAO.delete(tempMetricId);
    }

    public Metric getMetricById(int metricId) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        MetricDAO metricDao = factory.getMetricDAO();
        Metric metric = metricDao.read(metricId);
        return metric;
    }

    public Metric getMetricByIdForExist(int metricId) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        MetricDAO metricDao = factory.getMetricDAO();
        Metric metric = metricDao.readForExist(metricId);
        return metric;
    }
}