package pl.damianozga.eatbetter.service;

import pl.damianozga.eatbetter.dao.CaloriesDAO;
import pl.damianozga.eatbetter.dao.DAOFactory;
import pl.damianozga.eatbetter.dao.PlanDAO;
import pl.damianozga.eatbetter.dao.ProductDAO;
import pl.damianozga.eatbetter.model.Calories;
import pl.damianozga.eatbetter.model.Plan;
import pl.damianozga.eatbetter.model.Product;

import java.sql.SQLException;
import java.util.List;

public class PlanService {

    public void addPlan(String plan_name, double plan_protein, double plan_carbo, double plan_fat, double plan_fiber,
                        double plan_category, double plan_group, Calories calories) {
        Plan plan = createPlanObject(plan_name, plan_protein, plan_carbo, plan_fat, plan_fiber, plan_category,
                plan_group, calories);

        DAOFactory factory = DAOFactory.getDAOFactory();
        PlanDAO planDao = factory.getPlanDAO();
        planDao.create(plan);
    }

    private Plan createPlanObject(String plan_name, double plan_protein, double plan_carbo, double plan_fat, double plan_fiber,
                                  double plan_category, double plan_group, Calories calories) {
        Plan plan = new Plan();
        plan.setPlanName(plan_name);
        plan.setPlanProtein(plan_protein);
        plan.setPlanCarbo(plan_carbo);
        plan.setPlanFat(plan_fat);
        plan.setPlanFiber(plan_fiber);
        plan.setPlanCategory(plan_category);
        plan.setPlanGroup(plan_group);
        Calories copyCalories = new Calories(calories);
        plan.setCalories(copyCalories);
        return plan;
    }

    public void deletePlan(int caloriesId) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        PlanDAO planDao = factory.getPlanDAO();
        planDao.delete((long) caloriesId);
    }

    public List<Plan> getPlan(long calories_id) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        PlanDAO planDao = factory.getPlanDAO();
        List<Plan> plan = planDao.getAll(calories_id);
        return plan;
    }
}
