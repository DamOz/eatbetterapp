package pl.damianozga.eatbetter.service;

import pl.damianozga.eatbetter.dao.CaloriesDAO;
import pl.damianozga.eatbetter.dao.DAOFactory;
import pl.damianozga.eatbetter.dao.MetricDAO;
import pl.damianozga.eatbetter.model.Calories;
import pl.damianozga.eatbetter.model.Metric;

import java.sql.SQLException;

public class CaloriesService {

    public void addCalories(double calories_total, double calories_protein, double calories_meal_protein,
                            double calories_carbo, double calories_meal_carbo, double calories_fat,
                            double calories_meal_fat, double calories_fiber,
                            double calories_water, double calories_vegetables, Metric metric) {
        Calories calories = createCaloriesObject(calories_total, calories_protein, calories_meal_protein,
                calories_carbo, calories_meal_carbo, calories_fat, calories_meal_fat,
                calories_fiber, calories_water, calories_vegetables, metric);

        DAOFactory factory = DAOFactory.getDAOFactory();
        CaloriesDAO caloriesDAO = factory.getCaloriesDAO();
        caloriesDAO.create(calories);
    }

    private Calories createCaloriesObject(double calories_total, double calories_protein, double calories_meal_protein,
                                          double calories_carbo, double calories_meal_carbo, double calories_fat,
                                          double calories_meal_fat, double calories_fiber,
                                          double calories_water, double calories_vegetables, Metric metric) {
        Calories calories = new Calories();
        calories.setCaloriesTotal(calories_total);
        calories.setCaloriesProtein(calories_protein);
        calories.setCaloriesMealProtein(calories_meal_protein);
        calories.setCaloriesCarbo(calories_carbo);
        calories.setCaloriesMealCarbo(calories_meal_carbo);
        calories.setCaloriesFat(calories_fat);
        calories.setCaloriesMealFat(calories_meal_fat);
        calories.setCaloriesFiber(calories_fiber);
        calories.setCaloriesWater(calories_water);
        calories.setCaloriesVegetables(calories_vegetables);
        Metric metricCopy = new Metric(metric);
        calories.setMetric(metricCopy);
        return calories;
    }

    public void deleteCalories(int caloriesId) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        CaloriesDAO caloriesDAO = factory.getCaloriesDAO();
        caloriesDAO.delete((long) caloriesId);
    }


    public Calories getCaloriesById(long metric_id) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        CaloriesDAO caloriesDAO = factory.getCaloriesDAO();
        Calories calories = caloriesDAO.read(metric_id);
        return calories;
    }

    public Calories getCaloriesByIdForDelete(long metric_id) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        CaloriesDAO caloriesDAO = factory.getCaloriesDAO();
        Calories calories = caloriesDAO.readForDelete(metric_id);
        return calories;
    }

    public Calories getCaloriesByIdForExist(long metric_id) throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        CaloriesDAO caloriesDAO = factory.getCaloriesDAO();
        Calories calories = caloriesDAO.readForExist(metric_id);
        return calories;
    }


}
