package pl.damianozga.eatbetter.service;

import pl.damianozga.eatbetter.dao.DAOFactory;
import pl.damianozga.eatbetter.dao.PlanDAO;
import pl.damianozga.eatbetter.dao.RecipeDAO;
import pl.damianozga.eatbetter.model.Plan;
import pl.damianozga.eatbetter.model.Recipe;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class RecipeService {

    public void addRecipe(String recipeName, String recipeDescription, String recipeIg1, String recipeIg2,
                          String recipeIg3, String recipeIg4, String recipeIg5, String recipeIg6,
                          String recipeIg7, String recipeIg8, String recipeIg9, String recipeIg10,
                          String recipeImg, String recipeContent) {
        Recipe recipe = createRecipeObject(recipeName, recipeDescription, recipeIg1, recipeIg2, recipeIg3, recipeIg4,
                recipeIg5, recipeIg6, recipeIg7, recipeIg8, recipeIg9, recipeIg10, recipeImg, recipeContent);
        DAOFactory factory = DAOFactory.getDAOFactory();
        RecipeDAO recipeDAO = factory.getRecipeDAO();
        recipeDAO.create(recipe);
    }

    private Recipe createRecipeObject(String recipeName, String recipeDescription,  String recipeIg1, String recipeIg2,
                                      String recipeIg3, String recipeIg4, String recipeIg5, String recipeIg6,
                                      String recipeIg7, String recipeIg8, String recipeIg9, String recipeIg10,
                                      String recipeImg, String recipeContent) {
        Recipe recipe = new Recipe();
        recipe.setRecipeName(recipeName);
        recipe.setRecipeDescription(recipeDescription);
        recipe.setRecipeIg1(recipeIg1);
        recipe.setRecipeIg2(recipeIg2);
        recipe.setRecipeIg3(recipeIg3);
        recipe.setRecipeIg4(recipeIg4);
        recipe.setRecipeIg5(recipeIg5);
        recipe.setRecipeIg6(recipeIg6);
        recipe.setRecipeIg7(recipeIg7);
        recipe.setRecipeIg8(recipeIg8);
        recipe.setRecipeIg9(recipeIg9);
        recipe.setRecipeIg10(recipeIg10);
        recipe.setRecipeImg(recipeImg);
        recipe.setRecipeContent(recipeContent);
        recipe.setRecipeTimestamp(new Timestamp(new Date().getTime()));
        return recipe;
    }

    public List<Recipe> getRecipes() throws SQLException {
        DAOFactory factory = DAOFactory.getDAOFactory();
        RecipeDAO recipeDao = factory.getRecipeDAO();
        List<Recipe> recipes = recipeDao.getAll();
        return recipes;
    }


}

