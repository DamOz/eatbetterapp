package pl.damianozga.eatbetter.service;

import pl.damianozga.eatbetter.dao.DAOFactory;
import pl.damianozga.eatbetter.dao.ProductDAO;
import pl.damianozga.eatbetter.model.Product;

import java.util.List;

public class ProductService {

    public void addProduct(String productName, int productProtein, int productCarbo,
                           int productFat, int productFiber, int productCategory) {
        Product product = new Product();
        product.setProductName(productName);
        product.setProductProtein(productProtein);
        product.setProductCarbo(productCarbo);
        product.setProductFat(productFat);
        product.setProductFiber(productFiber);
        product.setProductCategory(productCategory);
        DAOFactory factory = DAOFactory.getDAOFactory();
        ProductDAO productDao = factory.getProductDAO();
        productDao.create(product);
    }

    public List<Product> getAllProducts() {
        DAOFactory factory = DAOFactory.getDAOFactory();
        ProductDAO productDao = factory.getProductDAO();
        List<Product> products = productDao.getAll();
        return products;
    }
}
