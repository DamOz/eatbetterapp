package pl.damianozga.eatbetter.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.eatbetter.model.Product;
import pl.damianozga.eatbetter.util.ConnectionProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductDAOImpl implements ProductDAO {

    private static final String CREATE_PRODUCT =
            "INSERT INTO product ( product_name, product_protein, product_carbo, product_fat, " +
                    "product_fiber, product_category )  " +
                    "VALUES ( :product_name, :product_protein, :product_carbo, :product_fat," +
                    " :product_fiber, :product_category);";

    private static final String READ_ALL_PRODUCT =
            "SELECT product_id, product_name, product_protein, product_carbo," +
                    " product_fat, product_fiber, product_category FROM product;";

    private NamedParameterJdbcTemplate template;

    public ProductDAOImpl() {

        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }

    @Override
    public Product create(Product product) {
        Product resultProduct = new Product(product);
        KeyHolder holder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("product_name", product.getProductName());
        paramMap.put("product_protein", product.getProductProtein());
        paramMap.put("product_carbo", product.getProductCarbo());
        paramMap.put("product_fat", product.getProductFat());
        paramMap.put("product_fiber", product.getProductFiber());
        paramMap.put("product_category", product.getProductCategory());
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_PRODUCT, paramSource, holder);
        if (update > 0) {
            resultProduct.setProductId(holder.getKey().longValue());
        }
        return resultProduct;
    }

    @Override
    public Product read(Integer primaryKey) {
        return null;
    }

    @Override
    public boolean update(Product updateObject) {
        return false;
    }

    @Override
    public void delete(Integer key) throws SQLException {
    }

    @Override
    public List<Product> getAll() {

        List<Product> products = template.query(READ_ALL_PRODUCT, new ProductRowMapper());
        return products;
    }

    @Override
    public Product getProductById(int id) {
        return null;
    }

    private class ProductRowMapper implements RowMapper<Product> {
        @Override
        public Product mapRow(ResultSet resultSet, int row) throws SQLException {
            Product product = new Product();
            product.setProductId(resultSet.getLong("product_id"));
            product.setProductName(resultSet.getString("product_name"));
            product.setProductProtein(resultSet.getInt("product_protein"));
            product.setProductCarbo(resultSet.getInt("product_carbo"));
            product.setProductFat(resultSet.getInt("product_fat"));
            product.setProductFiber(resultSet.getInt("product_fiber"));
            product.setProductCategory(resultSet.getInt("product_category"));
            return product;
        }
    }
}
