package pl.damianozga.eatbetter.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.eatbetter.model.Calories;
import pl.damianozga.eatbetter.model.Metric;
import pl.damianozga.eatbetter.util.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CaloriesDAOImpl implements CaloriesDAO {

    private static final String CREATE_CALORIES = "INSERT INTO calories( calories_total, calories_protein, calories_meal_protein," +
            " calories_carbo, calories_meal_carbo, calories_fat, calories_meal_fat," +
            "calories_water, calories_vegetables, calories_fiber, calories_metric_id)" +
            " VALUES ( :calories_total, :calories_protein, :calories_meal_protein, :calories_carbo, :calories_meal_carbo," +
            " :calories_fat, :calories_meal_fat, :calories_water, :calories_vegetables, :calories_fiber, :calories_metric_id);";

    private static final String READ_CALORIES = "SELECT metric_id, calories_id, calories_total, calories_protein, " +
            "calories_meal_protein, calories_carbo, calories_meal_carbo, calories_fat," +
            " calories_meal_fat, calories_fiber, calories_water, calories_vegetables," +
            "calories_metric_id FROM calories LEFT JOIN metric ON calories_metric_id=metric.metric_id " +
            "WHERE calories_metric_id = :metric_id;";

    private static final String READ_CALORIES_FOR_EXIST = "SELECT metric_id, calories_id, calories_total, calories_protein," +
            " calories_meal_protein, calories_carbo,calories_meal_carbo, calories_fat," +
            " calories_meal_fat, calories_fiber, calories_water, calories_vegetables," +
            " calories_metric_id FROM calories LEFT JOIN metric ON " +
            "calories_metric_id=metric.metric_id WHERE calories_metric_id = ";

    private static final String READ_CALORIES_FOR_DELETE = "SELECT metric_id, calories_id, calories_total, calories_protein, " +
            "calories_meal_protein, calories_carbo, calories_meal_carbo, calories_fat," +
            " calories_meal_fat, calories_fiber, calories_water, calories_vegetables, " +
            "calories_metric_id FROM calories LEFT JOIN metric ON " +
            "calories_metric_id=metric.metric_id WHERE calories_metric_id = :metric_id;";

    private static final String DELETE_CALORIES = "DELETE FROM calories WHERE calories_id = ";

    private NamedParameterJdbcTemplate template;

    public CaloriesDAOImpl() {

        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }

    @Override
    public Calories create(Calories calories) {
        Calories resultCalories = new Calories(calories);
        KeyHolder holder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("calories_total", calories.getCaloriesTotal());
        paramMap.put("calories_protein", calories.getCaloriesProtein());
        paramMap.put("calories_meal_protein", calories.getCaloriesMealProtein());
        paramMap.put("calories_carbo", calories.getCaloriesCarbo());
        paramMap.put("calories_meal_carbo", calories.getCaloriesMealCarbo());
        paramMap.put("calories_fat", calories.getCaloriesFat());
        paramMap.put("calories_meal_fat", calories.getCaloriesMealFat());
        paramMap.put("calories_fiber", calories.getCaloriesFiber());
        paramMap.put("calories_water", calories.getCaloriesWater());
        paramMap.put("calories_vegetables", calories.getCaloriesVegetables());
        paramMap.put("calories_metric_id", calories.getMetric().getMetricId());
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_CALORIES, paramSource, holder);
        if (update > 0) {
            resultCalories.setCaloriesId(holder.getKey().longValue());
        }
        return resultCalories;
    }

    @Override
    public Calories read(Long primaryKey) {
        Calories resultCalories;
        SqlParameterSource paramSource = new MapSqlParameterSource("metric_id", primaryKey);
        resultCalories = template.queryForObject(READ_CALORIES, paramSource, new CaloriesRowMapper());
        return resultCalories;
    }

    @Override
    public boolean update(Calories updateObject) {
        return false;
    }

    @Override
    public Calories readForDelete(long primaryKey) {
        Calories resultCalories;
        SqlParameterSource paramSource = new MapSqlParameterSource("metric_id", primaryKey);
        resultCalories = template.queryForObject(READ_CALORIES_FOR_DELETE, paramSource, new CaloriesRowMapper());
        return resultCalories;
    }

    @Override
    public Calories readForExist(long primaryKey) throws SQLException {
        String caloriesKey = primaryKey + ";";
        Connection conn = ConnectionProvider.getConnection();
        PreparedStatement prepStmt = conn.prepareStatement(READ_CALORIES_FOR_EXIST + caloriesKey);
        ResultSet rs = prepStmt.executeQuery();
        if (rs.first()) {
            Calories resultCalories;
            SqlParameterSource paramSource = new MapSqlParameterSource("metric_id", primaryKey);
            resultCalories = template.queryForObject(READ_CALORIES, paramSource, new CaloriesRowMapper());
            return resultCalories;
        } else
            return null;
    }

    @Override
    public void delete(Long key) throws SQLException {
        String caloriesKey = key + ";";
        Connection conn = ConnectionProvider.getConnection();
        PreparedStatement prepStmt = conn.prepareStatement(DELETE_CALORIES + caloriesKey);
        prepStmt.executeUpdate();
    }

    @Override
    public List<Calories> getAll() {
        return null;
    }

    private class CaloriesRowMapper implements RowMapper<Calories> {
        @Override
        public Calories mapRow(ResultSet resultSet, int row) throws SQLException {
            Calories calories = new Calories();
            calories.setCaloriesId(resultSet.getLong("calories_id"));
            calories.setCaloriesTotal(resultSet.getInt("calories_total"));
            calories.setCaloriesProtein(resultSet.getInt("calories_protein"));
            calories.setCaloriesMealProtein(resultSet.getInt("calories_meal_protein"));
            calories.setCaloriesCarbo(resultSet.getInt("calories_carbo"));
            calories.setCaloriesMealCarbo(resultSet.getInt("calories_meal_carbo"));
            calories.setCaloriesFat(resultSet.getInt("calories_fat"));
            calories.setCaloriesMealFat(resultSet.getInt("calories_meal_fat"));
            calories.setCaloriesFiber(resultSet.getInt("calories_fiber"));
            calories.setCaloriesWater(resultSet.getDouble("calories_water"));
            calories.setCaloriesVegetables(resultSet.getInt("calories_vegetables"));
            Metric metric = new Metric();
            calories.setMetric(metric);
            metric.setMetricId(resultSet.getLong("metric_id"));
            return calories;
        }
    }
}
