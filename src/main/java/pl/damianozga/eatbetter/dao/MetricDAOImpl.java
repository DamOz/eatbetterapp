package pl.damianozga.eatbetter.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.eatbetter.model.Metric;
import pl.damianozga.eatbetter.model.User;
import pl.damianozga.eatbetter.util.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetricDAOImpl implements MetricDAO {

    private static final String CREATE_METRIC = "INSERT INTO metric ( metric_goal, metric_age, metric_sex, metric_weight, metric_growth, " +
            "metric_hours, metric_work, metric_body, metric_meals, metric_redmeat, metric_poultry, metric_dairyproduct, metric_egg, metric_fish," +
            " metric_seafood, metric_legume, metric_nuts, metric_user_id ) VALUES ( :metric_goal, :metric_age, :metric_sex,:metric_weight,:metric_growth,:metric_hours," +
            ":metric_work,:metric_body,:metric_meals,:metric_redmeat,:metric_poultry,:metric_dairyproduct, :metric_egg, :metric_fish, " +
            ":metric_seafood,:metric_legume, :metric_nuts, :metric_user_id );";

    private static final String READ_METRIC = "SELECT username, user_id, metric_id, metric_goal, metric_age, metric_sex, metric_weight, metric_growth, " +
            "metric_hours, metric_work, metric_body, metric_meals, metric_redmeat, metric_poultry, metric_dairyproduct, " +
            "metric_egg, metric_fish, metric_seafood, metric_legume, metric_nuts, metric_user_id " +
            "FROM metric LEFT JOIN user ON metric_user_id=user.user_id WHERE metric_user_id= :user_id;";

    private static final String READ_METRIC_FOR_EXIST = "SELECT username, user_id, metric_id, metric_goal, metric_age, metric_sex, metric_weight, metric_growth, " +
            "metric_hours, metric_work, metric_body, metric_meals, metric_redmeat, metric_poultry, metric_dairyproduct, " +
            "metric_egg, metric_fish, metric_seafood, metric_legume, metric_nuts, metric_user_id " +
            "FROM metric LEFT JOIN user ON metric_user_id=user.user_id WHERE metric_user_id= ";

    private static final String READ_METRIC_FOR_DELETE = "SELECT username, user_id, metric_id, metric_goal, metric_age, metric_sex, metric_weight, metric_growth, " +
            " metric_hours, metric_work, metric_body, metric_meals, metric_redmeat, metric_poultry, metric_dairyproduct," +
            " metric_egg, metric_fish, metric_seafood, metric_legume, metric_nuts, metric_user_id " +
            "FROM metric LEFT JOIN user ON metric_user_id=user.user_id WHERE metric_id= :metric_id;";

    private static final String DELETE_METRIC = "DELETE FROM metric WHERE metric_id = ";

    private NamedParameterJdbcTemplate template;

    public MetricDAOImpl() {

        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }

    @Override
    public Metric create(Metric metric) {
        Metric resultMetric = new Metric(metric);
        KeyHolder holder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("metric_goal", metric.getMetricGoal());
        paramMap.put("metric_age", metric.getMetricAge());
        paramMap.put("metric_sex", metric.getMetricSex());
        paramMap.put("metric_weight", metric.getMetricWeight());
        paramMap.put("metric_growth", metric.getMetricGrowth());
        paramMap.put("metric_hours", metric.getMetricHours());
        paramMap.put("metric_work", metric.getMetricWork());
        paramMap.put("metric_body", metric.getMetricBody());
        paramMap.put("metric_meals", metric.getMetricMeals());
        paramMap.put("metric_redmeat", metric.getWantRedMeat());
        paramMap.put("metric_poultry", metric.getWantPoultry());
        paramMap.put("metric_dairyproduct", metric.getWantDairyProducts());
        paramMap.put("metric_egg", metric.getWantEggProducts());
        paramMap.put("metric_fish", metric.getWantFish());
        paramMap.put("metric_seafood", metric.getWantSeaFood());
        paramMap.put("metric_legume", metric.getWantLegume());
        paramMap.put("metric_nuts", metric.getWantNuts());
        paramMap.put("metric_user_id", metric.getUser().getId());
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_METRIC, paramSource, holder);
        if (update > 0) {
            resultMetric.setMetricId(holder.getKey().longValue());
        }
        return resultMetric;
    }

    @Override
    public Metric read(Integer primaryKey) {
        Metric resultMetric;
        SqlParameterSource paramSource = new MapSqlParameterSource("user_id", primaryKey);
        resultMetric = template.queryForObject(READ_METRIC, paramSource, new MetricRowMapper());
        return resultMetric;

    }

    @Override
    public Metric readForExist(Integer primaryKey) throws SQLException {
        String metricKey = primaryKey + ";";
        Connection conn = ConnectionProvider.getConnection();
        PreparedStatement prepStmt = conn.prepareStatement(READ_METRIC_FOR_EXIST + metricKey);
        ResultSet rs = prepStmt.executeQuery();
        if (rs.first()) {
            Metric resultMetric;
            SqlParameterSource paramSource = new MapSqlParameterSource("user_id", primaryKey);
            resultMetric = template.queryForObject(READ_METRIC, paramSource, new MetricRowMapper());
            return resultMetric;
        } else
            return null;
    }


    @Override
    public Metric readForDelete(Integer primaryKey) {
        Metric resultMetric;
        SqlParameterSource paramSource = new MapSqlParameterSource("metric_id", primaryKey);
        resultMetric = template.queryForObject(READ_METRIC_FOR_DELETE, paramSource, new MetricRowMapper());
        return resultMetric;
    }

    @Override
    public boolean update(Metric updateObject) {
        return false;
    }

    @Override
    public void delete(Integer key) throws SQLException {
        String metricKey = key + ";";
        Connection conn = ConnectionProvider.getConnection();
        PreparedStatement prepStmt = conn.prepareStatement(DELETE_METRIC + metricKey);
        prepStmt.executeUpdate();

    }

    @Override
    public List<Metric> getAll() {
        return null;
    }

    @Override
    public Metric getMetricByUserId(long id) {
        return null;
    }

    private class MetricRowMapper implements RowMapper<Metric> {
        @Override
        public Metric mapRow(ResultSet resultSet, int row) throws SQLException {
            Metric metric = new Metric();
            metric.setMetricId(resultSet.getLong("metric_id"));
            metric.setMetricGoal(resultSet.getInt("metric_goal"));
            metric.setMetricAge(resultSet.getInt("metric_age"));
            metric.setMetricSex(resultSet.getInt("metric_sex"));
            metric.setMetricWeight(resultSet.getInt("metric_weight"));
            metric.setMetricGrowth(resultSet.getInt("metric_growth"));
            metric.setMetricHours(resultSet.getInt("metric_hours"));
            metric.setMetricWork(resultSet.getInt("metric_work"));
            metric.setMetricBody(resultSet.getInt("metric_body"));
            metric.setMetricMeals(resultSet.getInt("metric_meals"));
            metric.setWantRedMeat(resultSet.getInt("metric_redmeat"));
            metric.setWantPoultry(resultSet.getInt("metric_poultry"));
            metric.setWantDairyProducts(resultSet.getInt("metric_dairyproduct"));
            metric.setWantEggProducts(resultSet.getInt("metric_egg"));
            metric.setWantFish(resultSet.getInt("metric_fish"));
            metric.setWantSeaFood(resultSet.getInt("metric_seafood"));
            metric.setWantLegume(resultSet.getInt("metric_legume"));
            metric.setWantNuts(resultSet.getInt("metric_nuts"));
            User user = new User();
            metric.setUser(user);
            user.setId(resultSet.getLong("user_id"));
            user.setUsername(resultSet.getString("username"));
            return metric;
        }
    }
}
