package pl.damianozga.eatbetter.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.eatbetter.model.Metric;
import pl.damianozga.eatbetter.model.Plan;
import pl.damianozga.eatbetter.model.Recipe;
import pl.damianozga.eatbetter.util.ConnectionProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecipeDAOImpl implements RecipeDAO {

    private static final String CREATE_RECIPE =
            "INSERT INTO recipe(recipe_name, recipe_description, recipe_ig1, recipe_ig2, " +
                    "recipe_ig3, recipe_ig4, recipe_ig5, recipe_ig6, recipe_ig7, recipe_ig8, recipe_ig9," +
                    "recipe_ig10, recipe_img, recipe_content, recipe_date) " +
                    "VALUES (:recipe_name, :recipe_description,  :recipe_ig1, :recipe_ig2, :recipe_ig3, :recipe_ig4," +
                    " :recipe_ig5, :recipe_ig6, :recipe_ig7, :recipe_ig8, :recipe_ig9, :recipe_ig10," +
                    " :recipe_img, :recipe_content, :recipe_date);";

    private static final String READ_ALL_RECIPE = "SELECT recipe_id, recipe_name, recipe_description, recipe_ig1, " +
            "recipe_ig2, recipe_ig3, recipe_ig4, recipe_ig5, recipe_ig6, recipe_ig7, recipe_ig8, recipe_ig9," +
            " recipe_ig10, recipe_img, recipe_content, recipe_date FROM recipe;";

    private NamedParameterJdbcTemplate template;

    public RecipeDAOImpl() {

        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }


    @Override
    public List<Recipe> getAll() {
        List<Recipe> resultRecipes;
        resultRecipes = template.query(READ_ALL_RECIPE, new RecipeRowMapper());
        return resultRecipes;
    }

    @Override
    public Recipe create(Recipe recipe) {
        Recipe resultRecipe = new Recipe(recipe);
        KeyHolder holder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("recipe_name", recipe.getRecipeName());
        paramMap.put("recipe_description", recipe.getRecipeDescription());
        paramMap.put("recipe_ig1", recipe.getRecipeIg1());
        paramMap.put("recipe_ig2", recipe.getRecipeIg2());
        paramMap.put("recipe_ig3", recipe.getRecipeIg3());
        paramMap.put("recipe_ig4", recipe.getRecipeIg4());
        paramMap.put("recipe_ig5", recipe.getRecipeIg5());
        paramMap.put("recipe_ig6", recipe.getRecipeIg6());
        paramMap.put("recipe_ig7", recipe.getRecipeIg7());
        paramMap.put("recipe_ig8", recipe.getRecipeIg8());
        paramMap.put("recipe_ig9", recipe.getRecipeIg9());
        paramMap.put("recipe_ig10", recipe.getRecipeIg10());
        paramMap.put("recipe_img", recipe.getRecipeImg());
        paramMap.put("recipe_content", recipe.getRecipeContent());
        paramMap.put("recipe_date", recipe.getRecipeTimestamp());
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_RECIPE, paramSource, holder);
        if (update > 0) {
            resultRecipe.setRecipeId(holder.getKey().longValue());
        }
        return resultRecipe;
    }

    @Override
    public Recipe read(Long primaryKey) {
        return null;
    }

    @Override
    public boolean update(Recipe updateObject) {
        return false;
    }

    @Override
    public void delete(Long key) throws SQLException {

    }


    private class RecipeRowMapper implements RowMapper<Recipe> {
        @Override
        public Recipe mapRow(ResultSet resultSet, int row) throws SQLException {
            Recipe recipe = new Recipe();
            recipe.setRecipeId(resultSet.getLong("recipe_id"));
            recipe.setRecipeName(resultSet.getString("recipe_name"));
            recipe.setRecipeDescription(resultSet.getString("recipe_description"));
            recipe.setRecipeIg1(resultSet.getString("recipe_ig1"));
            recipe.setRecipeIg2(resultSet.getString("recipe_ig2"));
            recipe.setRecipeIg3(resultSet.getString("recipe_ig3"));
            recipe.setRecipeIg4(resultSet.getString("recipe_ig4"));
            recipe.setRecipeIg5(resultSet.getString("recipe_ig5"));
            recipe.setRecipeIg6(resultSet.getString("recipe_ig6"));
            recipe.setRecipeIg7(resultSet.getString("recipe_ig7"));
            recipe.setRecipeIg8(resultSet.getString("recipe_ig8"));
            recipe.setRecipeIg9(resultSet.getString("recipe_ig9"));
            recipe.setRecipeIg10(resultSet.getString("recipe_ig10"));
            recipe.setRecipeImg(resultSet.getString("recipe_img"));
            recipe.setRecipeContent(resultSet.getString("recipe_content"));
            recipe.setRecipeTimestamp(resultSet.getTimestamp("recipe_date"));
            return recipe;
        }
    }
}
