package pl.damianozga.eatbetter.dao;

public class MysqlDAOFactory extends DAOFactory {

    @Override
    public UserDAO getUserDAO() {
        return new UserDAOImpl();
    }

    @Override
    public RecipeDAO getRecipeDAO() {
        return new RecipeDAOImpl();
    }

    @Override
    public ProductDAO getProductDAO() {
        return new ProductDAOImpl();
    }

    @Override
    public MetricDAO getMetricDAO() {
        return new MetricDAOImpl();
    }

    @Override
    public CaloriesDAO getCaloriesDAO() {
        return new CaloriesDAOImpl();
    }

    @Override
    public PlanDAO getPlanDAO() {
        return new PlanDAOImpl();
    }
}
