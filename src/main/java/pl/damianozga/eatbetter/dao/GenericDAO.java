package pl.damianozga.eatbetter.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

public interface GenericDAO<T, PK extends Serializable> {

    //CRUD
    T create(T newObject);

    T read(PK primaryKey) throws SQLException;

    boolean update(T updateObject);

    void delete(PK key) throws SQLException;

    List<T> getAll() throws SQLException;
}
