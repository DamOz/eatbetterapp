package pl.damianozga.eatbetter.dao;

import pl.damianozga.eatbetter.model.User;

import java.util.List;

public interface UserDAO extends GenericDAO<User, Long> {

    List<User> getAll();

    User getUserByUsername(String username);

    User getUserId(String username);

}
