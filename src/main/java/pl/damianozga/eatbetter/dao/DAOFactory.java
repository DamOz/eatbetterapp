package pl.damianozga.eatbetter.dao;

import pl.damianozga.eatbetter.exeption.NoSuchDbTypeException;

public abstract class DAOFactory {

    public static final int MYSQL_DAO_FACTORY = 1;

    public abstract UserDAO getUserDAO();

    public abstract RecipeDAO getRecipeDAO();

    public abstract ProductDAO getProductDAO();

    public abstract MetricDAO getMetricDAO();

    public abstract CaloriesDAO getCaloriesDAO();

    public abstract PlanDAO getPlanDAO();

    public static DAOFactory getDAOFactory() {
        DAOFactory factory = null;
        try {
            factory = getDAOFactory(MYSQL_DAO_FACTORY);
        } catch (NoSuchDbTypeException e) {
            e.printStackTrace();
        }
        return factory;
    }

    private static DAOFactory getDAOFactory(int type) throws NoSuchDbTypeException {
        switch (type) {
            case MYSQL_DAO_FACTORY:
                return new MysqlDAOFactory();
            default:
                throw new NoSuchDbTypeException();
        }
    }
}
