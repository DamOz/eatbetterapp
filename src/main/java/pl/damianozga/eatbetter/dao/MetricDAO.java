package pl.damianozga.eatbetter.dao;

import pl.damianozga.eatbetter.model.Metric;

import java.sql.SQLException;
import java.util.List;

public interface MetricDAO extends GenericDAO<Metric, Integer> {

    List<Metric> getAll();

    Metric readForDelete(Integer primaryKey);

    Metric readForExist(Integer primaryKey) throws SQLException;

    Metric getMetricByUserId(long id);

}
