package pl.damianozga.eatbetter.dao;

import pl.damianozga.eatbetter.model.Plan;

import java.sql.SQLException;
import java.util.List;

public interface PlanDAO extends GenericDAO<Plan, Long> {


    List<Plan> getAll(long calories_id) throws SQLException;
}
