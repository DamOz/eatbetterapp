package pl.damianozga.eatbetter.dao;

import pl.damianozga.eatbetter.model.Product;

import java.util.List;

public interface ProductDAO extends GenericDAO<Product, Integer> {

    List<Product> getAll();

    Product getProductById(int id);
}
