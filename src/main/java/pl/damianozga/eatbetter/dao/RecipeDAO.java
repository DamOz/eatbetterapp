package pl.damianozga.eatbetter.dao;

import pl.damianozga.eatbetter.model.Recipe;

import java.util.List;

public interface RecipeDAO extends GenericDAO<Recipe, Long> {

    List<Recipe> getAll();
}