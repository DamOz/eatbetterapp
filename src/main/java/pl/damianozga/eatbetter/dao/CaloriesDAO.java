package pl.damianozga.eatbetter.dao;

import pl.damianozga.eatbetter.model.Calories;

import java.sql.SQLException;
import java.util.List;

public interface CaloriesDAO extends GenericDAO<Calories, Long> {

    @Override
    List<Calories> getAll();

    Calories readForDelete(long primaryKey);

    Calories readForExist(long primaryKey) throws SQLException;
}
