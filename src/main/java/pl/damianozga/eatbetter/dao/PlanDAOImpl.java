package pl.damianozga.eatbetter.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.eatbetter.model.Calories;
import pl.damianozga.eatbetter.model.Plan;
import pl.damianozga.eatbetter.util.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlanDAOImpl implements PlanDAO {

    private static final String CREATE_PLAN = "INSERT INTO plan( plan_name, plan_protein, plan_carbo, plan_fat, plan_fiber," +
            "plan_category, plan_group, plan_calories_id) VALUES ( :plan_name, :plan_protein, :plan_carbo, :plan_fat, :plan_fiber, " +
            ":plan_category, :plan_group, :plan_calories_id);";

    private static final String READ_PLAN = "SELECT calories_id, plan_id, plan_name, plan_protein, plan_carbo, " +
            "plan_fat, plan_fiber, plan_category, plan_group, plan_calories_id FROM plan LEFT JOIN calories" +
            " ON plan_calories_id = calories.calories_id WHERE plan_calories_id = :calories_id";


    private static final String DELETE_PLAN = "DELETE FROM plan WHERE plan_calories_id = ";

    private NamedParameterJdbcTemplate template;

    public PlanDAOImpl() {

        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());

    }

    @Override
    public Plan create(Plan plan) {
        Plan resultPlan = new Plan(plan);
        KeyHolder holder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("plan_name", plan.getPlanName());
        paramMap.put("plan_protein", plan.getPlanProtein());
        paramMap.put("plan_carbo", plan.getPlanCarbo());
        paramMap.put("plan_fat", plan.getPlanFat());
        paramMap.put("plan_fiber", plan.getPlanFiber());
        paramMap.put("plan_category", plan.getPlanCategory());
        paramMap.put("plan_group", plan.getPlanGroup());
        paramMap.put("plan_calories_id", plan.getCalories().getCaloriesId());
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_PLAN, paramSource, holder);
        if (update > 0) {
            resultPlan.setPlanId(holder.getKey().longValue());
        }
        return resultPlan;
    }

    @Override
    public Plan read(Long primaryKey) throws SQLException {
        Plan resultPlan;
        SqlParameterSource paramSource = new MapSqlParameterSource("calories_id", primaryKey);
        resultPlan = template.queryForObject(READ_PLAN, paramSource, new PlanRowMapper());
        return resultPlan;
    }

    @Override
    public boolean update(Plan updateObject) {
        return false;
    }

    @Override
    public void delete(Long key) throws SQLException {
        String planKey = key + ";";
        Connection conn = ConnectionProvider.getConnection();
        PreparedStatement prepStmt = conn.prepareStatement(DELETE_PLAN + planKey);
        prepStmt.executeUpdate();
    }

    @Override
    public List<Plan> getAll() throws SQLException {
        return null;
    }

    @Override
    public List<Plan> getAll(long calories_id) throws SQLException {
        List<Plan> resultPlan;
        SqlParameterSource paramSource = new MapSqlParameterSource("calories_id", calories_id);
        resultPlan = template.query(READ_PLAN, paramSource, new PlanRowMapper());
        return resultPlan;
    }

    private class PlanRowMapper implements RowMapper<Plan> {
        @Override
        public Plan mapRow(ResultSet resultSet, int i) throws SQLException {
            Plan plan = new Plan();
            plan.setPlanId(resultSet.getLong("plan_id"));
            plan.setPlanName(resultSet.getString("plan_name"));
            plan.setPlanProtein(resultSet.getInt("plan_protein"));
            plan.setPlanCarbo(resultSet.getInt("plan_carbo"));
            plan.setPlanFat(resultSet.getInt("plan_fat"));
            plan.setPlanFiber(resultSet.getInt("plan_fiber"));
            plan.setPlanCategory(resultSet.getInt("plan_category"));
            plan.setPlanGroup(resultSet.getInt("plan_group"));
            Calories calories = new Calories();
            plan.setCalories(calories);
            calories.setCaloriesId(resultSet.getLong("calories_id"));
            return plan;
        }
    }
}
