<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-10-27
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- slideshow intro -->
<section>
    <header class="container eb-intro">
        <div class="eb-intro-slideshow">
            <img src="../resources/img/slideshow/salad.jpg">
            <img src="../resources/img/slideshow/salmon.jpg">
            <img src="../resources/img/slideshow/steak.jpg">
        </div>
        <div class="eb-intro-header">
            <h1>Eatbetter</h1>
            <p>Jedz lepiej dla lepszego samopoczucia</p>
        </div>
    </header>
</section>

<!-- recipes gallery-->

<section class="jumbotron text-center eb-recipes-header">
    <div class="container">
        <h1 class="text-uppercase mb-3">Propozycje przepisów</h1>
        <h2>Nie masz pomysłu, co dzisiaj ugotować? Nie przejmuj się. Skorzystaj z poniższych przepisów, na pewno
            wybierzesz coś dla siebie.</h2>
    </div>


    <div class="py-5 eb-recipes-gallery">
        <div class="container">
            <div class="row">
                <c:forEach var="recipe" items="${requestScope.recipes}">
                    <div class="col-sm-6 col-md-4">
                        <div class="card mb-4 eb-recipes-card">
                            <img src="<c:out value="${recipe.recipeImg}"/>" alt="recipe-photo">
                            <p class="eb-recipe-title"><c:out value="${recipe.recipeName}"/></p>
                            <div class="card-body">
                                <p class="card-text"><c:out value="${recipe.recipeDescription}"/></p>
                                <div class="d-flex justify-content-between align-items-left">
                                    <div class="btn-group">
                                        <details>
                                            <summary class="btn btn-sm eb-recipes-card-btn">Zobacz przepis</summary>
                                            <ul class="text-justify">
                                                <c:if test="${not empty recipe.recipeIg1}">
                                                <li><c:out value="${recipe.recipeIg1}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg2}">
                                                    <li><c:out value="${recipe.recipeIg2}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg3}">
                                                    <li><c:out value="${recipe.recipeIg3}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg4}">
                                                    <li><c:out value="${recipe.recipeIg4}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg5}">
                                                    <li><c:out value="${recipe.recipeIg5}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg6}">
                                                    <li><c:out value="${recipe.recipeIg6}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg7}">
                                                    <li><c:out value="${recipe.recipeIg7}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg8}">
                                                    <li><c:out value="${recipe.recipeIg8}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg9}">
                                                    <li><c:out value="${recipe.recipeIg9}"/></li>
                                                </c:if>
                                                <c:if test="${not empty recipe.recipeIg10}">
                                                    <li><c:out value="${recipe.recipeIg10}"/></li>
                                                </c:if>
                                            </ul>
                                            <p class="text-justify"><c:out value="${recipe.recipeContent}"/></p>
                                        </details>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</section>


<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>
