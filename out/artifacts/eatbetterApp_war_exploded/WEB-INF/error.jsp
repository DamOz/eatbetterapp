[<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Eatbetteerror</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>



<!-- error -->
<section>
    <div class="col eb-signin text-center mb-5">
        <div class="col-sm-8 col-md-6 col-lg-4 mx-auto d-block eb-signin-form">
            <h1> Wystąpił błąd wróć na <br> <a href="/" class="text-warning">stronę główną</a></h1>
            <p class="mt-5 mb-3">© Eatbetter 2020</p>
        </div>
    </div>
</section>


</body>
</html>
