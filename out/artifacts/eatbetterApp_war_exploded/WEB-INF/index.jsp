<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="./resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./resources/css/microtip.css">
    <link rel="stylesheet" type="text/css" href="./resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- slideshow intro -->
<section>
    <header class="container-fluid eb-intro">
        <div class="eb-intro-slideshow">
            <img src="./resources/img/slideshow/salad.jpg">
            <img src="./resources/img/slideshow/salmon.jpg">
            <img src="./resources/img/slideshow/steak.jpg">
        </div>
        <div class="eb-intro-header">
            <h1>Eatbetter</h1>
            <p>Jedz lepiej dla lepszego samopoczucia</p>
        </div>
    </header>
</section>

<!-- info -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 eb-welcome-info">
                <h1 class="text-left text-uppercase mt-5 ml-4">Witamy w świecie <span class="eb-welcome-info-a">
                    lepszego żywienia się </span></h1>
                <h2> Aplikacji webowej, która wygeneruje Ci plan żywieniowy w kilka sekund.</h2>
                <div class="eb-welcome-info-tut text-center">
                    <h3>STWÓRZ SWÓJ PLAN ŻYWIENIOWY</h3>
                    <img class="img-fluid d-block mx-auto" src="./resources/icon/down-arrow-welcome.png">
                    <h4 class="text-uppercase">Zarejestruj się</h4> <a href="/register">REJESTRACJA</a>
                    <img class="img-fluid d-block mx-auto" src="./resources/icon/down-arrow-welcome.png">
                    <h4 class="text-uppercase">Zaloguj się</h4><a href="/login">ZALOGUJ SIĘ</a>
                    <img class="img-fluid d-block mx-auto" src="./resources/icon/down-arrow-welcome.png">
                    <h5 class="text-uppercase">Jeżeli jesteś już zalogowany to możesz teraz wygenerować swój
                        spersonalizowany plan
                        żywieniowy.</h5>
                    <a href="/metric">GENERUJ PLAN</a>
                    <img class="img-fluid d-block mx-auto" src="./resources/icon/down-arrow-welcome.png">
                    <h5 class="text-uppercase">Po uzupełnieniu i potwierdzeniu danych plan generuje się automatycznie
                        i jest gotowy do pobrania w pliku PDF. Plan żywieniowy możesz pobrać w zakładce TWÓJ PLAN. Plik
                        zacznie ściągać się po kliknięciu przycisku POBIERZ PLAN W PLIKU PDF</h5>
                    <a href="/plan">TWÓJ PLAN</a>

                    <p class="text-uppercase eb-welcome-info-b">"Jesteś tym, co jesz. Twoje samopoczucie jutro
                        jest zależne od tego, co dzisiaj masz na talerzu."</p>
                </div>
            </div>
            <div class="col-4 d-none d-lg-block eb-welcome-pic">
                <img class="img-fluid eb-welcom-pic-1" src="./resources/img/pics/product-cloud-3.png">
                <img class="img-fluid eb-welcom-pic-1" src="./resources/img/pics/product-cloud.png">
            </div>

        </div>

    </div>
</section>

<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="./resources/js/slider.js"></script>
</body>
</html>
