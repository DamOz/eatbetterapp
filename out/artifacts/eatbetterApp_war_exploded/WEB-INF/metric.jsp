<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-10-27
  Time: 18:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/microtip.css">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- generation form -->

    <div class="container-fluid eb-generation">
        <form class="container-fluid" action="metric" method="post">
            <div class="col eb-generation-header text-center mb-5">
                <h1 class="text-uppercase mb-3">Podaj dane do planu</h1>
                <h2> Do wygenerowania planu potrzebuje od Ciebie poniższych danych w innym przypadku wygenerowanie planu
                    będzie niemożliwe.</h2>
            </div>
            <div class="container-fluid eb-generation-a mb-5">
                <div class="row">
                    <div class="col-xs-11 col-sm-10 col-md-8 col-lg-5 d-block mr-auto ml-auto mb-5 mb-lg-0 eb-generation-form">
                        <h3 class="text-center mb-3"> Informacje potrzebne do określenia kaloryczności oraz gramatury
                            posiłków</h3>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Jaki jest Twój cel?</label>
                            <div class="col">
                                <label aria-label="Co chcesz osiągnąć przez zastosowanie planu żywieniowego?"
                                       data-microtip-position="top" role="tooltip"/>
                                <select name="InputMetricGoal" class="custom-select mr-sm-2" required>
                                    <option value="1">Redukcja masy ciała</option>
                                    <option value="2">Zwiększenie masy ciała</option>
                                    <option value="3">Utrzymanie stanu obecnego</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Ile masz lat?</label>
                            <label class="col col-form-label"
                                   aria-label="Twój wiek potrzebny jest do obliczenia zapotrzebowania kalorycznego"
                                   data-microtip-position="top" role="tooltip"/>
                            <input type="number" class="form-control" name="InputMetricAge" min="18" max="65"
                                   required>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Jaka jest Twoja płeć?</label>
                            <div class="col">
                                    <div class="col-4 d-inline mx-auto form-check">
                                        <label class="form-check-label">
                                            <label aria-label="Twoja płeć potrzebna jest do obliczenia zapotrzebowania kalorycznego"
                                                   data-microtip-position="top" role="tooltip"/>
                                            <input type="radio" class="form-check-input" value="1" name="InputMetricSex"
                                                   required><span class="eb-generation-input">Kobieta</span>
                                        </label>
                                    </div>
                                    <div class="col-4 d-inline mx-auto form-check">
                                        <label class="form-check-label">
                                            <label aria-label="Twoja płeć potrzebna jest do obliczenia zapotrzebowania kalorycznego"
                                                   data-microtip-position="top" role="tooltip"/>
                                            <input type="radio" class="form-check-input" value="0" name="InputMetricSex"
                                                   required><span class="eb-generation-input">Mężczyzna</span>
                                        </label>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Jaka jest Twoja aktualna
                                waga?</label>
                            <label class="col col-form-label"
                                   aria-label="Twoja waga  potrzebna jest do obliczenia zapotrzebowania kalorycznego"
                                   data-microtip-position="top" role="tooltip"/>
                            <input type="number" class="form-control" name="InputMetricWeight" min="35"
                                   max="170" required>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Ile masz wzrostu?</label>
                            <label class="col col-form-label"
                                   aria-label="Twój wzrost potrzebny jest do obliczenia zapotrzebowania kalorycznego"
                                   data-microtip-position="top" role="tooltip"/>
                            <input type="number" class="form-control" name="InputMetricGrowth" min="120"
                                   max="230" required>

                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Ile godzin w tygodniu
                                trenujesz?</label>

                            <label class="col col-form-label"
                                   aria-label="Ilość godzin treningowych znacznie wpływa na zapotrzebowanie kaloryczne"
                                   data-microtip-position="top" role="tooltip"/>
                            <select name="InputMetricHours" class="custom-select mr-sm-2" required>
                                <option value="0">Nie trenuje</option>
                                <option value="1">1h - 3h (dwa treningi)</option>
                                <option value="2">4h - 6h (trzy treningi)</option>
                                <option value="3">7h - 9h (cztery treningi)</option>
                                <option value="4">10h - 12h (pięć treningów)</option>
                                <option value="5">13h - 15h (sześć treningów)</option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Jaki jest typ twojej
                                pracy?</label>

                            <label class="col col-form-label"
                                   aria-label="Tryb pracy mocno wpływa na zapotrzebowanie kaloryczne"
                                   data-microtip-position="top" role="tooltip"/>
                            <select name="InputMetricWork" class="custom-select mr-sm-2" required>
                                <option value="1">Nie pracuje</option>
                                <option value="2">Praca siedząca</option>
                                <option value="3">Praca stojąca, nie fizyczna</option>
                                <option value="4">Lekka praca fizyczna</option>
                                <option value="5">Ciężka praca fizyczna</option>
                            </select>

                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Jaki jest Twój typ budowy
                                ciała?</label>

                            <label class="col col-form-label text-justify" aria-label="Ektomorfik - sylwetka szczupła, problem z
                                                    nabraniem masy ciała |
                                                   Mezomorfik - sylwetka umięśniona, łatwość w budowaniu masy mięśniowej |
                                                   Endomorfik - sylwetka okazała, łatwość w nabieraniu tkanki tłuszczowej"
                                   data-microtip-position="top" data-microtip-size="large" role="tooltip"/>
                            <select name="InputMetricBody" class="custom-select mr-sm-2" id="inlineFormCustomSelect"
                                    required>
                                <option value="1">Ektomorfik</option>
                                <option value="2">Mezomorfik</option>
                                <option value="3">Endomorfik</option>
                            </select>

                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label eb-generation-form-text">Ile chciałbyś jeść posiłków
                                dziennie?</label>
                            <div class="col">
                                <div class="row">
                                    <div class="col-1 mx-auto form-check">
                                        <label class="form-check-label">
                                            <label aria-label="Na tyle posiłków policzę Ci gramaturę"
                                                                               data-microtip-position="top" role="tooltip"/>
                                            <input type="radio" class="form-check-input" value="3"
                                                   name="InputMetricMeals" required><span
                                                class="eb-generation-input">3</span>
                                        </label>
                                    </div>
                                    <div class="col-1 mx-auto form-check">
                                        <label class="form-check-label">
                                            <label aria-label="Na tyle posiłków policzę Ci gramaturę"
                                                   data-microtip-position="top" role="tooltip"/>
                                            <input type="radio" class="form-check-input" value="4"
                                                   name="InputMetricMeals" required><span
                                                class="eb-generation-input">4</span>
                                        </label>
                                    </div>
                                    <div class="col-1 mx-auto form-check">
                                        <label class="form-check-label">
                                            <label aria-label="Na tyle posiłków policzę Ci gramaturę"
                                                   data-microtip-position="top" role="tooltip"/>
                                            <input type="radio" class="form-check-input" value="5"
                                                   name="InputMetricMeals" required><span
                                                class="eb-generation-input">5</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- additional info -->

                    <div class="col-xs-11 col-sm-10 col-md-8 col-lg-5 d-block mx-auto eb-generation-form">
                        <h3 class="text-center mb-5">Twoje preferencje żywieniowe, czego
                            <span class="text-uppercase text-danger">nie chcesz</span> mieć w planie</h3>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Mięso czerwone</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputMetricRedMeat">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Drób</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputMetricPoultry">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Nabiał</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputMetricDairyProducts">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Jaja kurze</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputEggProducts">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Ryby</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputMetricFish">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Owoce morza</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputMetricSeaFood">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Warzywa strączkowe</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputMetricLegume">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ml-5 mb-4">
                            <label class="col col-form-label eb-generation-form-text">Orzechy</label>
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value="1"
                                               name="InputMetricNuts">
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-4 text-center mx-auto mb-5">
                <button class="btn eb-generate-btn text-uppercase py-2 px-4" type="submit">Przejdź dalej</button>
            </div>
        </form>
    </div>



<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>
