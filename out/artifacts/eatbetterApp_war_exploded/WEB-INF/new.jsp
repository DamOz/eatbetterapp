<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020-11-04
  Time: 17:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eatbetter for better well-being</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../resources/style/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/css/fontello.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jspf"/>

<!-- new recipe form -->

<section>
    <div class="col eb-signin text-center mb-5">
        <form class="col-sm-12 col-md-8 col-lg-6 mx-auto d-block eb-signin-form" action="recipe" method="post">
            <h1 class="mb-3">Śmiało, dodaj nowy przepis!</h1>
            <label class="sr-only mb-3 ">Nazwa potrawy</label>
            <input placeholder="Nazwa" type="text" name="inputRecipeName" class="form-control eb-signin-input mb-3"
                   required autofocus>
            <label class="sr-only mb-3 ">Krótki opis potrawy</label>
            <input placeholder="Opis" type="text" name="inputRecipeDescription"
                   class="form-control eb-signin-input mb-3" required autofocus>
            <label class="sr-only mb-3 ">Krótki opis potrawy</label>
            <input placeholder="Składnik 1" type="text" name="inputRecipeIg1"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 2" type="text" name="inputRecipeIg2"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 3" type="text" name="inputRecipeIg3"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 4" type="text" name="inputRecipeIg4"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 5" type="text" name="inputRecipeIg5"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 6" type="text" name="inputRecipeIg6"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 7" type="text" name="inputRecipeIg7"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 8" type="text" name="inputRecipeIg8"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 9" type="text" name="inputRecipeIg9"
                   class="form-control eb-signin-input mb-3" autofocus>
            <input placeholder="Składnik 10" type="text" name="inputRecipeIg10"
                   class="form-control eb-signin-input mb-3" autofocus>
            <label class="sr-only mb-3 ">Sposób przygotowania</label>
            <textarea placeholder="Sposób przygotowania" rows="10" type="text" name="inputRecipeContent"
                      class="form-control eb-signin-input mb-3" required autofocus></textarea>
            <button class="btn btn-lg btn-block eb-form-btn" type="submit">Dodaj przepis</button>
        </form>
    </div>
</section>


<!-- footer -->

<jsp:include page="fragment/footer.jspf"/>

<!-- javascript -->
<script src="../resources/js/slider.js"></script>
</body>
</html>